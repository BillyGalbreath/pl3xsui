package net.pl3x.bukkit.pl3xsui;

import net.pl3x.bukkit.pl3xsui.api.selection.Selection;
import net.pl3x.bukkit.pl3xsui.api.selection.shape.Cuboid;
import net.pl3x.bukkit.pl3xsui.api.selection.shape.Cylinder;
import net.pl3x.bukkit.pl3xsui.api.selection.shape.Ellipsoid;
import net.pl3x.bukkit.pl3xsui.api.selection.shape.Polygon2D;
import net.pl3x.bukkit.pl3xsui.api.selection.shape.Polyhedron;
import net.pl3x.bukkit.pl3xsui.configuration.Config;
import net.pl3x.bukkit.pl3xsui.configuration.PlayerConfig;
import net.pl3x.bukkit.pl3xsui.task.CuboidParticlesTask;
import net.pl3x.bukkit.pl3xsui.task.CylinderParticlesTask;
import net.pl3x.bukkit.pl3xsui.task.EllipsoidParticlesTask;
import net.pl3x.bukkit.pl3xsui.task.ParticlesTask;
import net.pl3x.bukkit.pl3xsui.task.Polygon2DParticlesTask;
import net.pl3x.bukkit.pl3xsui.task.PolyhedronParticlesTask;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Wraps a Bukkit Player to hold Selection data
 */
@SuppressWarnings({"WeakerAccess", "UnusedReturnValue", "unused"})
public class Pl3xPlayer {
    private static final HashMap<UUID, Pl3xPlayer> players = new HashMap<>();

    /**
     * Get all players
     *
     * @return Collection of Pl3xPlayers
     */
    public static Collection<Pl3xPlayer> getPlayers() {
        return players.values();
    }

    /**
     * Get the Pl3xPlayer instance for a Player
     *
     * @param player Bukkit Player
     * @return Pl3xPlayer instance
     */
    public static Pl3xPlayer getPlayer(Player player) {
        return getPlayer(player.getUniqueId());
    }

    /**
     * Get the Pl3xPlayer instance for a Player
     *
     * @param uuid Bukkit Player's UUID
     * @return Pl3xPlayer instance
     */
    public static Pl3xPlayer getPlayer(UUID uuid) {
        if (!players.containsKey(uuid)) {
            players.put(uuid, new Pl3xPlayer(uuid));
        }
        return players.get(uuid);
    }

    /**
     * Remove Pl3xPlayer instance from memory
     *
     * @param player Bukkit Player
     */
    public static void remove(Player player) {
        remove(player.getUniqueId());
    }

    /**
     * Remove Pl3xPlayer instance from memory
     *
     * @param uuid Bukkit Player's UUID
     */
    public static void remove(UUID uuid) {
        if (players.containsKey(uuid)) {
            players.remove(uuid);
        }
    }

    /**
     * Unloads all Pl3xPlayer data from memory
     */
    public static void unloadAll() {
        players.values().forEach(Pl3xPlayer::discard);

        players.clear();
    }

    private UUID uuid;
    private Selection selection;
    private ParticlesTask selectionVisual;
    private final Set<Player> watchers = new HashSet<>();

    private Pl3xPlayer(UUID uuid) {
        this.uuid = uuid;
    }

    /**
     * Discard all data in memory about this player
     */
    public void discard() {
        if (selection != null) {
            selection.clear();
        }

        if (selectionVisual != null) {
            selectionVisual.cancel();
        }

        uuid = null;
        selection = null;
        selectionVisual = null;
    }

    /**
     * Get this player's Bukkit UUID
     *
     * @return UUID
     */
    public UUID getUUID() {
        return uuid;
    }

    /**
     * Get the Bukkit Player instance for this player
     *
     * @return Bukkit Player
     */
    public Player getPlayer() {
        return Bukkit.getPlayer(uuid);
    }

    /**
     * Get the player's wand selection
     *
     * @return Selection
     */
    public Selection getSelection() {
        return selection;
    }

    /**
     * Set a player's selection
     *
     * @param selection Selection to set
     * @return Selection that was set
     */
    public Selection setSelection(Selection selection) {
        return this.selection = selection;
    }

    /**
     * Shows the player their current selection by sending them particle effects
     */
    public void showSelection() {
        hideSelection(); // hide previous selection if it was showing

        // Show nothing if current selection is not set
        if (selection == null) {
            return;
        }

        if (!PlayerConfig.ENABLED.getBoolean(getUUID())) {
            return;
        }

        // show current selection if one exists
        if (selection instanceof Cuboid) {
            selectionVisual = new CuboidParticlesTask(getPlayer(), (Cuboid) selection);
        } else if (selection instanceof Ellipsoid) {
            selectionVisual = new EllipsoidParticlesTask(getPlayer(), (Ellipsoid) selection);
        } else if (selection instanceof Cylinder) {
            selectionVisual = new CylinderParticlesTask(getPlayer(), (Cylinder) selection);
        } else if (selection instanceof Polygon2D) {
            selectionVisual = new Polygon2DParticlesTask(getPlayer(), (Polygon2D) selection);
        } else if (selection instanceof Polyhedron) {
            selectionVisual = new PolyhedronParticlesTask(getPlayer(), (Polyhedron) selection);
        }

        runVisualTask();
    }

    /**
     * Shows the player their current selection by sending them customized particle effects
     *
     * @param particlesTask ParticleTask to run
     */
    public void showCustomSelection(ParticlesTask particlesTask) {
        hideSelection(); // hide previous selection if it was showing

        // show custom visual
        selectionVisual = particlesTask;

        runVisualTask();
    }

    /**
     * Stops sending the player particle effects for their current selection
     */
    public void hideSelection() {
        if (selectionVisual == null) {
            return;  // nothing to cancel
        }

        // cancel task and free up memory
        selectionVisual.cancel();
        selectionVisual = null;
    }

    /**
     * Get all watchers of this player
     *
     * @return Set of all watchers
     */
    public Set<Player> getWatchers() {
        return watchers;
    }

    /**
     * Check if watching a certain player
     *
     * @param player Player to check
     * @return True if watching
     */
    public boolean isWatching(Player player) {
        return watchers.contains(player);
    }

    /**
     * Add watcher
     *
     * @param player Player to add
     */
    public void addWatcher(Player player) {
        watchers.add(player);
    }

    /**
     * Remove watcher
     *
     * @param player Player to remove
     */
    public void removeWatcher(Player player) {
        watchers.remove(player);
    }

    private void runVisualTask() {
        if (selectionVisual == null) {
            Logger.debug("Selection is set, but could not create visual task!");
            return;
        }
        selectionVisual.runTaskTimerAsynchronously(Pl3xSUI.getPlugin(Pl3xSUI.class), 5, Config.PARTICLE_DELAY.getInt());
    }
}
