package net.pl3x.bukkit.pl3xsui.configuration;

import net.pl3x.bukkit.pl3xsui.Logger;
import net.pl3x.bukkit.pl3xsui.Pl3xSUI;
import net.pl3x.bukkit.pl3xsui.api.ParticleColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.UUID;

/**
 * Player configuration
 */
public enum PlayerConfig {
    ENABLED(true),
    PARTICLE_TYPE("default"),
    COLOR_CUBOID_EDGE("default"),
    COLOR_CUBOID_FILL("default"),
    COLOR_CYLINDER_EDGE("default"),
    COLOR_CYLINDER_FILL("default"),
    COLOR_SPHERE_EDGE("default"),
    COLOR_SPHERE_FILL("default"),
    COLOR_ELLIPSOID_EDGE("default"),
    COLOR_ELLIPSOID_FILL("default"),
    COLOR_POLYGON2D_EDGE("default"),
    COLOR_POLYGON2D_FILL("default"),
    COLOR_POLYHEDRON_EDGE("default"),
    COLOR_POLYHEDRON_FILL("default");

    private final Pl3xSUI plugin;
    private final Object def;

    PlayerConfig(Object def) {
        this.plugin = Pl3xSUI.getPlugin(Pl3xSUI.class);
        this.def = def;
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    /**
     * Set new value in config
     *
     * @param uuid  UUID of player
     * @param value New value to set
     */
    public void set(UUID uuid, Object value) {
        saveConfig(uuid, value);
    }

    /**
     * Get boolean value
     *
     * @param uuid UUID of player
     * @return boolean value
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean getBoolean(UUID uuid) {
        return getConfig(uuid).getBoolean(getKey(), (Boolean) def);
    }

    /**
     * Get String value
     *
     * @param uuid UUID of player
     * @return String value
     */
    public String getString(UUID uuid) {
        return getConfig(uuid).getString(getKey(), (String) def);
    }

    /**
     * Get ParticleColor value
     * <p>
     * If no value is set then default from config.yml is used
     *
     * @param uuid UUID of player
     * @return ParticleColor value
     */
    public ParticleColor getColor(UUID uuid) {
        String colorName = getConfig(uuid).getString(getKey(), (String) def).toUpperCase();
        return ParticleColor.getColor(colorName.equals("DEFAULT") ? Config.valueOf(name()).getString() : colorName);
    }

    private YamlConfiguration getConfig(UUID uuid) {
        YamlConfiguration config = new YamlConfiguration();
        try {
            config.load(new File(plugin.getDataFolder(), "userdata" + File.separator + uuid.toString() + ".yml"));
        } catch (Exception e) {
            // do nothing! we don't save a config file for every player that joins!
        }
        return config;
    }

    private void saveConfig(UUID uuid, Object value) {
        YamlConfiguration config = new YamlConfiguration();
        try {
            File dir = new File(plugin.getDataFolder(), "userdata");
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    Logger.error("Unable to create player data directory: " + dir.getAbsolutePath());
                    return;
                }
            }
            File file = new File(dir, uuid.toString() + ".yml");
            if (!file.exists()) {
                if (!file.createNewFile()) {
                    Logger.error("Unable to create player data file: " + file.getAbsolutePath());
                    return;
                }
            }
            config.load(file);
            config.set(getKey(), value);
            config.save(file);
        } catch (Exception e) {
            Logger.error("Unable to save player data: " + uuid.toString() + ": " + getKey() + ": " + value);
        }
    }
}
