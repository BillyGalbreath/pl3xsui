package net.pl3x.bukkit.pl3xsui.configuration;

import net.pl3x.bukkit.pl3xsui.Pl3xSUI;
import net.pl3x.bukkit.pl3xsui.api.ParticleColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class ColorConfig extends YamlConfiguration {
    private static ColorConfig config;

    public static ColorConfig getConfig() {
        if (config == null) {
            config = new ColorConfig();
        }
        return config;
    }

    public static void saveDefaultConfig() {
        Pl3xSUI.getPlugin(Pl3xSUI.class).saveResource("colors.yml", false);
        getConfig();
    }

    public static void loadColors() {
        for (String key : config.getKeys(false)) {
            ParticleColor.addColor(key, config.getString(key));
        }
    }

    private final File file;
    private final Object saveLock = new Object();

    private ColorConfig() {
        super();
        file = new File(Pl3xSUI.getPlugin(Pl3xSUI.class).getDataFolder(), "colors.yml");
        load();
    }

    private void load() {
        synchronized (saveLock) {
            try {
                this.load(file);
            } catch (Exception ignore) {
            }
        }
    }
}
