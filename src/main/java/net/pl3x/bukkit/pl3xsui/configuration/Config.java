package net.pl3x.bukkit.pl3xsui.configuration;

import net.pl3x.bukkit.pl3xsui.Pl3xSUI;
import net.pl3x.bukkit.pl3xsui.api.ParticleColor;

import java.util.List;

/**
 * Plugin configuration
 */
@SuppressWarnings("unused")
public enum Config {
    COLOR_LOGS(true),
    DEBUG_MODE(false),
    LANGUAGE_FILE("lang-en.yml"),
    VIEW_LONG_DISTANCE(false),
    VIEW_RADIUS(32),
    PARTICLE_COUNT_THRESHOLD(1000000),
    PARTICLE_TYPE("REDSTONE"),
    PARTICLE_DELAY(20),
    CLEANUP_CONVEX(true),
    COLOR_CUBOID_EDGE("BLUE"),
    COLOR_CUBOID_FILL("YELLOW"),
    COLOR_CYLINDER_EDGE("BLUE"),
    COLOR_CYLINDER_FILL("YELLOW"),
    COLOR_SPHERE_EDGE("BLUE"),
    COLOR_SPHERE_FILL("YELLOW"),
    COLOR_ELLIPSOID_EDGE("BLUE"),
    COLOR_ELLIPSOID_FILL("YELLOW"),
    COLOR_POLYGON2D_EDGE("BLUE"),
    COLOR_POLYGON2D_FILL("YELLOW"),
    COLOR_POLYHEDRON_EDGE("BLUE"),
    COLOR_POLYHEDRON_FILL("YELLOW"),
    ALLOWED_PLAYER_PARTICLES(null),
    GRIEFPREVENTION_VISUALS(false);

    private final Pl3xSUI plugin;
    private final Object def;

    Config(Object def) {
        this.plugin = Pl3xSUI.getPlugin(Pl3xSUI.class);
        this.def = def;
    }

    /**
     * Reload config
     */
    public static void reload() {
        Pl3xSUI.getPlugin(Pl3xSUI.class).reloadConfig();
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    /**
     * Get boolean value
     *
     * @return boolean value
     */
    public boolean getBoolean() {
        return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
    }

    /**
     * Get int value
     *
     * @return int value
     */
    public int getInt() {
        return plugin.getConfig().getInt(getKey(), (int) def);
    }

    /**
     * Get String value
     *
     * @return String value
     */
    public String getString() {
        return plugin.getConfig().getString(getKey(), (String) def);
    }

    /**
     * Get list of Strings
     *
     * @return List of Strings
     */
    public List<String> getStringList() {
        return plugin.getConfig().getStringList(getKey());
    }

    /**
     * Get ParticleColor value
     *
     * @return ParticleColor value
     */
    public ParticleColor getColor() {
        return ParticleColor.getColor(plugin.getConfig().getString(getKey(), (String) def).toUpperCase());
    }
}
