package net.pl3x.bukkit.pl3xsui.configuration;

import net.pl3x.bukkit.pl3xsui.Logger;
import net.pl3x.bukkit.pl3xsui.Pl3xSUI;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

/**
 * Localization language configuration
 */
public enum Lang {
    UNKNOWN_SUBCOMMAND("&4Unknown Subcommand."),
    UNKNOWN_ERROR("&4An unknown error has happened."),
    PLAYER_COMMAND("&4This command is only available to players."),
    COMMAND_NO_PERMISSION("&4You do not have permission for this command."),
    PLAYER_NOT_FOUND("&4Player not found."),
    PLAYER_NOT_SPECIFIED("&4Must specify a player."),
    SELECTION_TOO_LARGE("&4Your selection is too large to render."),

    AVAILABLE_SUBCOMMANDS("&dAvailable Subcommands:"),
    COMMAND_DESCRIPTION("  &3{command} &e- &7{description}"),

    RELOAD("&d{plugin} v{version} reloaded."),

    SUI_TOGGLED("&dYou have toggled SUI to &7{toggle}&d."),

    STARTED_WATCHING_PLAYER("&dNow watching &7{player}&d."),
    STOPPED_WATCHING_PLAYER("&dNo longer watching &7{player}&d."),
    NOT_WATCHING_PLAYER("&4Not watching &7{player}&4."),
    ALREADY_WATCHING_PLAYER("&4Already watching &7{player}&4."),
    WATCHING_PLAYERS("&dWatching: &7{players}"),
    NOT_WATCHING_ANY_PLAYERS("&dNot watching any players."),

    PARTICLE_TYPE_DOES_NOT_EXIST("&4Particle type does not exist."),
    PARTICLE_TYPE_NOT_ALLOWED("&4Particle type not allowed."),
    PARTICLE_TYPE_RESET("&dParticle type reset."),
    PARTICLE_TYPE_SET("&dParticle type set to &7{type}&d."),

    COLOR_DOES_NOT_EXIST("&4That color doesn't exist."),
    CUBOID_COLOR_FILL_RESET("&dCuboid fill color reset."),
    CUBOID_COLOR_EDGE_RESET("&dCuboid edge color reset."),
    CYLINDER_COLOR_FILL_RESET("&dCylinder fill color reset."),
    CYLINDER_COLOR_EDGE_RESET("&dCylinder edge color reset."),
    SPHERE_COLOR_FILL_RESET("&dSphere fill color reset."),
    SPHERE_COLOR_EDGE_RESET("&dSphere edge color reset."),
    ELLIPSOID_COLOR_FILL_RESET("&dEllipsoid fill color reset."),
    ELLIPSOID_COLOR_EDGE_RESET("&dEllipsoid edge color reset."),
    POLYGON2D_COLOR_FILL_RESET("&dPolygon fill color reset."),
    POLYGON2D_COLOR_EDGE_RESET("&dPolygon edge color reset."),
    POLYHEDRON_COLOR_FILL_RESET("&dConvex fill color reset."),
    POLYHEDRON_COLOR_EDGE_RESET("&dConvex edge color reset."),
    CUBOID_COLOR_FILL_SET("&dCuboid fill color set to &7{color}&d."),
    CUBOID_COLOR_EDGE_SET("&dCuboid edge color set to &7{color}&d."),
    CYLINDER_COLOR_FILL_SET("&dCylinder fill color set to &7{color}&d."),
    CYLINDER_COLOR_EDGE_SET("&dCylinder edge color set to &7{color}&d."),
    SPHERE_COLOR_FILL_SET("&dSphere fill color set to &7{color}&d."),
    SPHERE_COLOR_EDGE_SET("&dSphere edge color set to &7{color}&d."),
    ELLIPSOID_COLOR_FILL_SET("&dEllipsoid fill color set to &7{color}&d."),
    ELLIPSOID_COLOR_EDGE_SET("&dEllipsoid edge color set to &7{color}&d."),
    POLYGON2D_COLOR_FILL_SET("&dPolygon fill color set to &7{color}&d."),
    POLYGON2D_COLOR_EDGE_SET("&dPolygon edge color set to &7{color}&d."),
    POLYHEDRON_COLOR_FILL_SET("&dConvex fill color set to &7{color}&d."),
    POLYHEDRON_COLOR_EDGE_SET("&dConvex edge color set to &7{color}&d."),

    DESC_CMD_PL3XSUI("Selection controls"),
    DESC_CMD_PARTICLE("Set particle type"),
    DESC_CMD_RELOAD("Reload plugin configs"),
    DESC_CMD_TOGGLE("Toggle SUI on/off"),
    DESC_CMD_COLOR("Set your personal selection colors"),
    DESC_CMD_COLOR_CUBOID("Control cuboid colors"),
    DESC_CMD_COLOR_CUBOID_FILL("Control cuboid fill color"),
    DESC_CMD_COLOR_CUBOID_EDGE("Control cuboid edge color"),
    DESC_CMD_COLOR_CYLINDER("Control cylinder colors"),
    DESC_CMD_COLOR_CYLINDER_FILL("Control cylinder fill color"),
    DESC_CMD_COLOR_CYLINDER_EDGE("Control cylinder edge color"),
    DESC_CMD_COLOR_SPHERE("Control sphere colors"),
    DESC_CMD_COLOR_SPHERE_FILL("Control sphere fill color"),
    DESC_CMD_COLOR_SPHERE_EDGE("Control sphere edge color"),
    DESC_CMD_COLOR_ELLIPSOID("Control ellipsoid colors"),
    DESC_CMD_COLOR_ELLIPSOID_FILL("Control ellipsoid fill color"),
    DESC_CMD_COLOR_ELLIPSOID_EDGE("Control ellipsoid edge color"),
    DESC_CMD_COLOR_POLYGON2D("Control polygon colors"),
    DESC_CMD_COLOR_POLYGON2D_FILL("Control polygon fill color"),
    DESC_CMD_COLOR_POLYGON2D_EDGE("Control polygon edge color"),
    DESC_CMD_COLOR_POLYHEDRON("Control convex colors"),
    DESC_CMD_COLOR_POLYHEDRON_FILL("Control convex fill color"),
    DESC_CMD_COLOR_POLYHEDRON_EDGE("Control convex edge color"),
    DESC_CMD_WATCHING("Control which player selections to view"),
    DESC_CMD_WATCHING_LIST("List people currently watching"),
    DESC_CMD_WATCHING_ADD("Start watching player"),
    DESC_CMD_WATCHING_REMOVE("Stop watching player"),

    HELP_CMD_PARTICLE("&dSet particle type\n   &e/&7sui particle type"),
    HELP_CMD_RELOAD("&dReload plugin configs\n/sui reload"),
    HELP_CMD_TOGGLE("&dToggle SUI on/off\n   &e/&7sui toggle"),
    HELP_CMD_COLOR_CUBOID_FILL("&dSet cuboid fill color\n   &e/&7sui color cuboid fill color"),
    HELP_CMD_COLOR_CUBOID_EDGE("&dSet cuboid edge color\n   &e/&7sui color cuboid edge color"),
    HELP_CMD_COLOR_CYLINDER_FILL("&dSet cylinder fill color\n   &e/&7sui color cylinder fill color"),
    HELP_CMD_COLOR_CYLINDER_EDGE("&dSet cylinder edge color\n   &e/&7sui color cylinder edge color"),
    HELP_CMD_COLOR_SPHERE_FILL("&dSet sphere fill color\n   &e/&7sui color sphere fill color"),
    HELP_CMD_COLOR_SPHERE_EDGE("&dSet sphere edge color\n   &e/&7sui color sphere edge color"),
    HELP_CMD_COLOR_ELLIPSOID_FILL("&dSet ellipsoid fill color\n   &e/&7sui color ellipsoid fill color"),
    HELP_CMD_COLOR_ELLIPSOID_EDGE("&dSet ellipsoid edge color\n   &e/&7sui color ellipsoid edge color"),
    HELP_CMD_COLOR_POLYGON2D_FILL("&dSet polygon fill color\n   &e/&7sui color polygon fill color"),
    HELP_CMD_COLOR_POLYGON2D_EDGE("&dSet polygon edge color\n   &e/&7sui color polygon edge color"),
    HELP_CMD_COLOR_POLYHEDRON_FILL("&dSet convex fill color\n   &e/&7sui color convex fill color"),
    HELP_CMD_COLOR_POLYHEDRON_EDGE("&dSet convex edge color\n   &e/&7sui color convex edge color"),
    HELP_CMD_WATCHING_LIST("&dList people currently watching\n   &e/&7sui watch list"),
    HELP_CMD_WATCHING_ADD("&dStart watching player\n   &e/&7sui watch add player"),
    HELP_CMD_WATCHING_REMOVE("&dStop watching player\n   &e/&7sui watch remove player");

    private final String def;

    private static File configFile;
    private static FileConfiguration config;

    Lang(String def) {
        this.def = def;
        reload();
    }

    /**
     * Reload config if not already loaded
     * <p>
     * Same as doing reload(false)
     */
    public static void reload() {
        reload(false);
    }

    /**
     * Reload config
     *
     * @param force True to force reload
     */
    public static void reload(boolean force) {
        if (configFile == null || force) {
            String lang = Config.LANGUAGE_FILE.getString();
            Logger.debug("Loading language file: " + lang);
            configFile = new File(Pl3xSUI.getPlugin(Pl3xSUI.class).getDataFolder(), lang);
            if (!configFile.exists()) {
                Pl3xSUI.getPlugin(Pl3xSUI.class).saveResource(Config.LANGUAGE_FILE.getString(), false);
            }
        }
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    /**
     * Get String value
     *
     * @return String value
     */
    @Override
    public String toString() {
        String value = config.getString(name());
        if (value == null) {
            value = config.getString(getKey());
        }
        if (value == null) {
            Logger.warn("Missing lang data in file: " + getKey());
            value = def;
        }
        if (value == null) {
            Logger.error("Missing default lang data: " + getKey());
            value = "&c[missing lang data]";
        }
        return ChatColor.translateAlternateColorCodes('&', value);
    }

    /**
     * Replace text in this Lang
     *
     * @param find    String to replace
     * @param replace String to replace with
     * @return String value
     */
    public String replace(String find, String replace) {
        return toString().replace(find, replace);
    }
}
