package net.pl3x.bukkit.pl3xsui.api.selection.shape;

import net.pl3x.bukkit.pl3xsui.api.selection.Selection;
import org.bukkit.util.Vector;

/**
 * Represents an ellipsoid or sphere shape
 */
@SuppressWarnings("UnusedReturnValue")
public class Ellipsoid implements Selection, Cloneable {
    private Vector center;
    private Vector radius;

    /**
     * Get the center coordinates
     *
     * @return Center vector coordinates
     */
    public Vector getCenter() {
        return center;
    }

    /**
     * Set the center coordinates
     *
     * @param center New center coordinates
     * @return This ellipsoid shape
     */
    public Ellipsoid setCenter(Vector center) {
        this.center = center;
        return this;
    }

    /**
     * Get the radius values
     * <p>
     * The returned vector will contain the radius for each axis on x,y,z
     *
     * @return Radius values
     */
    public Vector getRadius() {
        return radius;
    }

    /**
     * Set the radius values
     * <p>
     * The supplied vector will contain the radius for each axis on x,y,z
     *
     * @return This ellipsoid shape
     */
    public Ellipsoid setRadius(Vector radius) {
        this.radius = radius;
        return this;
    }

    /**
     * Clears the data for this shape
     */
    @Override
    public void clear() {
        center = null;
        radius = null;
    }

    /**
     * Get a copy of this shape
     *
     * @return Copy of this shape
     */
    @Override
    public Ellipsoid clone() {
        try {
            return (Ellipsoid) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error(e);
        }
    }

    /**
     * Get a human readable String representation of this shape
     *
     * @return Human readable String representation of shape
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Ellipsoid[");
        if (center == null) {
            sb.append("null");
        } else {
            sb.append("center:[");
            sb.append(center.getX());
            sb.append(",");
            sb.append(center.getY());
            sb.append(",");
            sb.append(center.getZ());
            sb.append("], radius:[");
            if (radius == null) {
                sb.append("null");
            } else {
                sb.append(radius.getX());
                sb.append(",");
                sb.append(radius.getY());
                sb.append(",");
                sb.append(radius.getZ());
            }
            sb.append("]");
        }
        return sb.append("]").toString();
    }
}
