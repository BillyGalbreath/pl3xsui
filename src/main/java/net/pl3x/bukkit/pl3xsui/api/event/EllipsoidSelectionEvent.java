package net.pl3x.bukkit.pl3xsui.api.event;

import net.pl3x.bukkit.pl3xsui.api.selection.shape.Ellipsoid;
import org.bukkit.entity.Player;

/**
 * Event triggered when ellipsoid or sphere shape selection is about to display
 */
public class EllipsoidSelectionEvent extends SelectionEvent {
    private final Ellipsoid ellipsoid;

    /**
     * Create new ellipsoid selection event
     *
     * @param player    Player to show selection
     * @param ellipsoid Ellipsoid shape to show player
     */
    public EllipsoidSelectionEvent(Player player, Ellipsoid ellipsoid) {
        super(player);
        this.ellipsoid = ellipsoid;
    }

    /**
     * Get the ellipsoid shape
     *
     * @return Ellipsoid shape
     */
    public Ellipsoid getEllipsoid() {
        return ellipsoid;
    }
}
