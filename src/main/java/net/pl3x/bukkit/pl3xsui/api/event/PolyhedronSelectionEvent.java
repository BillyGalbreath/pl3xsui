package net.pl3x.bukkit.pl3xsui.api.event;

import net.pl3x.bukkit.pl3xsui.api.selection.shape.Polyhedron;
import org.bukkit.entity.Player;

/**
 * Event triggered when polyhedron (convex) shape selection is about to display
 */
public class PolyhedronSelectionEvent extends SelectionEvent {
    private final Polyhedron polyhedron;

    /**
     * Create new polyhedron (convex) selection event
     *
     * @param player     Player to show selection
     * @param polyhedron Polyhedron (convex) shape to show player
     */
    public PolyhedronSelectionEvent(Player player, Polyhedron polyhedron) {
        super(player);
        this.polyhedron = polyhedron;
    }

    /**
     * Get the polyhedron (convex) shape
     *
     * @return Polyhedron (convex) shape
     */
    public Polyhedron getPolyhedron() {
        return polyhedron;
    }
}
