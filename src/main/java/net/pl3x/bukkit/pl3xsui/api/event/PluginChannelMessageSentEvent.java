package net.pl3x.bukkit.pl3xsui.api.event;

import org.bukkit.entity.Player;

/**
 * Event triggered when server sends a message to a player on a plugin channel
 */
public class PluginChannelMessageSentEvent extends SelectionEvent {
    private final String channel;
    private final String message;

    /**
     * Create new sent message event.
     * <p>
     * You should not create/fake these unless you know what you're doing.
     *
     * @param player  Player that received the message from server
     * @param channel Channel the message was sent on
     * @param message The message sent
     */
    public PluginChannelMessageSentEvent(Player player, String channel, String message) {
        super(player);
        this.channel = channel;
        this.message = message;
    }

    /**
     * Get the channel the message was sent on
     *
     * @return The channel name
     */
    public String getchannel() {
        return channel;
    }

    /**
     * Get the message that was sent
     *
     * @return The message sent
     */
    public String getMessage() {
        return message;
    }
}
