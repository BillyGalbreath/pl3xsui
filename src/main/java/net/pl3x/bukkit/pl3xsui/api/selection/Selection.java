package net.pl3x.bukkit.pl3xsui.api.selection;

/**
 * Interface for selection shapes
 */
public interface Selection {
    /**
     * Clears the data for this shape
     */
    void clear();

    /**
     * Get a copy of this selection shape
     *
     * @return Copy of this selection shape
     */
    Selection clone();
}
