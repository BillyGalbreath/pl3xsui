package net.pl3x.bukkit.pl3xsui.api.event;

import net.pl3x.bukkit.pl3xsui.api.selection.shape.Cylinder;
import org.bukkit.entity.Player;

/**
 * Event triggered when cylinder shape selection is about to display
 */
public class CylinderSelectionEvent extends SelectionEvent {
    private final Cylinder cylinder;

    /**
     * Create new cylinder selection event
     *
     * @param player   Player to show selection
     * @param cylinder Cylinder shape to show player
     */
    public CylinderSelectionEvent(Player player, Cylinder cylinder) {
        super(player);
        this.cylinder = cylinder;
    }

    /**
     * Get the cylinder shape
     *
     * @return Cylinder shape
     */
    public Cylinder getCylinder() {
        return cylinder;
    }
}
