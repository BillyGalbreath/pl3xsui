package net.pl3x.bukkit.pl3xsui.api;

import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers.Particle;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings({"SameParameterValue", "unused"})
public interface IParticleTask {
    /**
     * Get the player this task is rendering for
     *
     * @return Player viewing shape
     */
    Player getPlayer();

    /**
     * Get the particle packets stored in this task
     *
     * @return Particle packets
     */
    ConcurrentHashMap<PacketContainer, Vector> getPackets();

    /**
     * Check is long distance mode is enabled
     *
     * @return True is long distance is enabled
     */
    boolean isLongDistance();

    /**
     * Set whether long distance is enabled
     *
     * @param longDistance Long distance boolean to set
     */
    void setLongDistance(boolean longDistance);

    /**
     * Get the viewing radius
     *
     * @return Viewing radius
     */
    int getRadius();

    /**
     * Set the viewing radius
     *
     * @param radius Radius to set
     */
    void setRadius(int radius);

    /**
     * Get the viewing radius squared (radius * radius)
     *
     * @return Squared radius
     */
    int getRadiusSquared();

    /**
     * Get the color of the fill pattern
     *
     * @return Fill color
     */
    ParticleColor getFillColor();

    /**
     * Set the color of the fill pattern
     *
     * @param fillColor Fill color
     */
    void setFillColor(ParticleColor fillColor);

    /**
     * Get the color of the edge pattern
     *
     * @return Edge color
     */
    ParticleColor getEdgeColor();

    /**
     * Set the color of the edge pattern
     *
     * @param edgeColor Edge color
     */
    void setEdgeColor(ParticleColor edgeColor);

    /**
     * Get the particle type
     *
     * @return Particle type
     */
    Particle getParticleType();

    /**
     * Set the particle type
     *
     * @param particleType Particle type
     */
    void setParticleType(Particle particleType);

    /**
     * Check if task is scheduled to be cancelled
     *
     * @return True if task is cancelling
     */
    boolean isCancelled();

    /**
     * Set whether to cancel task
     *
     * @param cancel Cancel state
     */
    void setCancelled(boolean cancel);

    /**
     * Check if the particle shape positions have already been calculated
     *
     * @return True if calculations have started running (not necessarily finished)
     */
    boolean hasAlreadyCalculated();

    /**
     * Set whether the particle shape positions are started calculating
     * <p>
     * You should never need to call this method. Setting this to false on a running
     * task will cause all calculations to start again, doubling the results
     *
     * @param alreadyCalculated Calculated state
     */
    void setAlreadyCalculated(boolean alreadyCalculated);

    /**
     * Add a parsed packet to the packets list to be rendered later
     *
     * @param vector Position of particle
     * @param color  Color of particle
     */
    void addPacket(Vector vector, ParticleColor color);

    /**
     * Check the color value against the particle type
     * <p>
     * If the particle is determined to not handle colors the value will return 0
     * otherwise the value will return the supplied color value
     *
     * @param color Red green of blue color value to check
     * @return Color value if particle type supports color, otherwise 0
     */
    float checkColor(float color);

    /**
     * Check if particle type is known to cause server/client crashes
     *
     * @return True if particle is known to cause server/client crashes
     */
    boolean crashPrevention();
}
