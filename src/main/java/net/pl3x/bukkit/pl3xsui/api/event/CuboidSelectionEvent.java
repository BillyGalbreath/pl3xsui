package net.pl3x.bukkit.pl3xsui.api.event;

import net.pl3x.bukkit.pl3xsui.api.selection.shape.Cuboid;
import org.bukkit.entity.Player;

/**
 * Event triggered when cuboid shape selection is about to display
 */
public class CuboidSelectionEvent extends SelectionEvent {
    private final Cuboid cuboid;

    /**
     * Create new cuboid selection event
     *
     * @param player Player to show selection
     * @param cuboid Cuboid shape to show player
     */
    public CuboidSelectionEvent(Player player, Cuboid cuboid) {
        super(player);
        this.cuboid = cuboid;
    }

    /**
     * Get the cuboid shape
     *
     * @return Cuboid shape
     */
    public Cuboid getCuboid() {
        return cuboid;
    }
}
