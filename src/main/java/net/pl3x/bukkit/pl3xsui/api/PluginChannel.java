package net.pl3x.bukkit.pl3xsui.api;

import com.comphenix.protocol.events.PacketContainer;
import org.bukkit.entity.Player;

/**
 * Interface for handling plugin channels
 */
public interface PluginChannel {
    /**
     * Register a player as a listener on a plugin channel
     *
     * @param player  Player to register
     * @param channel Channel to listen on
     */
    void registerPlayerChannel(Player player, String channel);

    /**
     * Get a packet's contents
     * <p>
     * Warning: This method is designed to read plugin channel packets only.
     * Trying to use a different packet will likely lead to errors.
     *
     * @param packet PluginChannel packet (custom payload)
     * @return String representation of the packet's contents
     */
    String getPacketContents(PacketContainer packet);
}
