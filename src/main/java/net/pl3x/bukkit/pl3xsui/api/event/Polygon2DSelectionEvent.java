package net.pl3x.bukkit.pl3xsui.api.event;

import net.pl3x.bukkit.pl3xsui.api.selection.shape.Polygon2D;
import org.bukkit.entity.Player;

/**
 * Event triggered when polygon2d shape selection is about to display
 */
public class Polygon2DSelectionEvent extends SelectionEvent {
    private final Polygon2D polygon2d;

    /**
     * Create new polygon2d selection event
     *
     * @param player    Player to show selection
     * @param polygon2d Polygon2D shape to show player
     */
    public Polygon2DSelectionEvent(Player player, Polygon2D polygon2d) {
        super(player);
        this.polygon2d = polygon2d;
    }

    /**
     * Get the polygon2d shape
     *
     * @return Polygon2D shape
     */
    public Polygon2D getPolygon2D() {
        return polygon2d;
    }
}
