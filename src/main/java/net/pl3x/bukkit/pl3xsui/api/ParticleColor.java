package net.pl3x.bukkit.pl3xsui.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a particle color
 */
@SuppressWarnings("WeakerAccess")
public class ParticleColor {
    public static final Map<String, ParticleColor> colors = new HashMap<>();

    private final float red;
    private final float green;
    private final float blue;

    /**
     * Creates a new particle color from RGB values 0 - 255
     *
     * @param red   Red value
     * @param green Green value
     * @param blue  Blue value
     */
    public ParticleColor(int red, int green, int blue) {
        // values of 0 are changed to 0.0001 for black
        // due to the way the particle packet works
        this.red = (red == 0 ? 0.0001F : red) / 255;
        this.green = (green == 0 ? 0.0001F : green) / 255;
        this.blue = (blue == 0 ? 0.0001F : blue) / 255;
    }

    /**
     * Get the red value
     * <p>
     * The values 0.0 - 1.0 represent 0 - 255
     *
     * @return Red value
     */
    public float getRed() {
        return red;
    }

    /**
     * Get the green value
     * <p>
     * The values 0.0 - 1.0 represent 0 - 255
     *
     * @return Green value
     */
    public float getGreen() {
        return green;
    }

    /**
     * Get the blue value
     * <p>
     * The values 0.0 - 1.0 represent 0 - 255
     *
     * @return Blue value
     */
    public float getBlue() {
        return blue;
    }

    /**
     * Get the hexadecimal color code for this color
     *
     * @return Hexadecimal color code
     */
    public String getHex() {
        return String.format("#%02x%02x%02x", (int) (red * 255), (int) (green * 255), (int) (blue * 255)).toUpperCase();
    }

    /**
     * Get a human readable String representation of this color
     *
     * @return Human readable String representation of color
     */
    @Override
    public String toString() {
        return "ParticleColor[red:[" +
                red +
                "], green:[" +
                green +
                "], blue:[" +
                blue +
                "]]";
    }

    /**
     * Get color by name or hex code
     * <p>
     * Invalid colors will default to RED
     *
     * @param color Color name or hex code
     * @return ParticleColor
     */
    public static ParticleColor getColor(String color) {
        ParticleColor actual = getColorExact(color);
        return actual == null ? new ParticleColor(255, 85, 85) : actual;
    }

    /**
     * Get color by name or hex code
     * <p>
     * Invalid colors return NULL
     *
     * @param color Color name or hex code
     * @return ParticleColor
     */
    public static ParticleColor getColorExact(String color) {
        ParticleColor particleColor = getColorFromName(color);

        if (particleColor != null) {
            return particleColor;
        }

        if (color.startsWith("#")) {
            color = color.substring(1); // remove # sign if present
        }

        if (color.length() < 6) {
            return null;
        }

        try {
            particleColor = new ParticleColor(
                    Integer.valueOf(color.substring(0, 2), 16),
                    Integer.valueOf(color.substring(2, 4), 16),
                    Integer.valueOf(color.substring(4, 6), 16)
            );
        } catch (Exception ignore) {
        }

        return particleColor;
    }

    /**
     * Gets the color from an internal list of names
     *
     * @param name Name to search for
     * @return ParticleColor with matching name or null if none found
     */
    public static ParticleColor getColorFromName(String name) {
        name = name.toLowerCase().replace("_", "-");
        for (Map.Entry<String, ParticleColor> entry : colors.entrySet()) {
            if (name.equals(entry.getKey())) {
                return entry.getValue();
            }
        }
        return null;
    }

    public static List<String> getNames() {
        List<String> names = new ArrayList<>();
        names.addAll(colors.keySet());
        Collections.sort(names);
        return names;
    }

    /**
     * Add a color
     *
     * @param name Color name
     * @param hex  Color hex code
     */
    public static void addColor(String name, String hex) {
        ParticleColor color = getColorExact(hex);
        if (color == null) {
            return;
        }
        colors.put(name.toLowerCase().replace("_", "-"), color);
    }
}
