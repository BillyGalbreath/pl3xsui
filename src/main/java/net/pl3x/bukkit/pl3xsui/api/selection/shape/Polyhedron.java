package net.pl3x.bukkit.pl3xsui.api.selection.shape;

import net.pl3x.bukkit.pl3xsui.api.selection.Selection;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a polyhedron (convex) shape
 */
@SuppressWarnings({"UnusedReturnValue", "unused"})
public class Polyhedron implements Selection, Cloneable {
    private List<Vector> points = new ArrayList<>();
    private List<Vector> faces = new ArrayList<>();

    /**
     * Get list of points
     *
     * @return List of vector coordinates
     */
    public List<Vector> getPoints() {
        return points;
    }

    /**
     * Set a new list of points
     *
     * @param points List of points
     * @return This polyhedron (convex) shape
     */
    public Polyhedron setPoints(List<Vector> points) {
        this.points = points;
        return this;
    }

    /**
     * Add a point at specific index
     * <p>
     * If the index position is greater that the size of the list of points the addition will fail
     *
     * @param index Index position
     * @param point Point to add
     * @return This polyhedron (convex) shape. Will return null if the addition failed
     */
    public Polyhedron addPoint(int index, Vector point) {
        if (points.size() > index) {
            return null;
        }
        points.add(point);
        return this;
    }

    /**
     * Get a point
     *
     * @param index Index position of point to get
     * @return Vector coordinates of point
     */
    public Vector getPoint(int index) {
        return points == null || points.size() <= index ? null : points.get(index);
    }

    /**
     * Get the list of faces
     * <p>
     * Each vector in the list represents an index in the list of points. The three
     * points represent the points of the surface face.
     *
     * @return List of faces
     */
    public List<Vector> getFaces() {
        return faces;
    }

    /**
     * Set a new list of faces
     * <p>
     * Each vector in the list represents an index in the list of points. The three
     * points represent the points of the surface face.
     *
     * @param faces List of faces
     * @return This polyhedron (convex) shape
     */
    public Polyhedron setFaces(List<Vector> faces) {
        this.faces = faces;
        return this;
    }

    /**
     * Add a new face
     * <p>
     * The vector represents an index in the list of points. The three
     * points represent the points of the surface face.
     *
     * @param face Face to add
     * @return This polyhedron (convex) shape
     */
    public Polyhedron addFace(Vector face) {
        faces.add(face);
        return this;
    }

    /**
     * Clears the data for this shape
     */
    @Override
    public void clear() {
        points.clear();
        faces.clear();
    }

    /**
     * Get a copy of this shape
     *
     * @return Copy of this shape
     */
    @Override
    public Polyhedron clone() {
        try {
            return (Polyhedron) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error(e);
        }
    }

    /**
     * Get a human readable String representation of this shape
     *
     * @return Human readable String representation of shape
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(256);
        sb.append("Polyhedron[");
        if (points == null || points.isEmpty()) {
            sb.append("null");
        } else {
            sb.append("points[");
            for (int i = 0; i < points.size(); i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append("p");
                sb.append(i);
                sb.append(":[");
                Vector point = points.get(i);
                if (point == null) {
                    sb.append("null");
                } else {
                    sb.append(point.getX());
                    sb.append(",");
                    sb.append(point.getY());
                    sb.append(",");
                    sb.append(point.getZ());
                }
                sb.append("]");
            }
            sb.append("], faces[");
            if (faces == null || faces.isEmpty()) {
                sb.append("null");
            } else {
                for (int i = 0; i < faces.size(); i++) {
                    if (i > 0) {
                        sb.append(", ");
                    }
                    sb.append("p");
                    sb.append(i);
                    sb.append(":[");
                    Vector face = faces.get(i);
                    if (face == null) {
                        sb.append("null");
                    } else {
                        sb.append(face.getX());
                        sb.append(",");
                        sb.append(face.getY());
                        sb.append(",");
                        sb.append(face.getZ());
                    }
                    sb.append("]");
                }
            }
            sb.append("]");
        }
        return sb.append("]").toString();
    }
}
