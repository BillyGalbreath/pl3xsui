package net.pl3x.bukkit.pl3xsui.api.selection.shape;

import net.pl3x.bukkit.pl3xsui.api.selection.Selection;
import org.bukkit.util.Vector;

/**
 * Represents a cylinder shape
 */
public class Cylinder implements Selection, Cloneable {
    private Vector center;
    private double radiusX;
    private double radiusZ;
    private int minY;
    private int maxY;

    /**
     * Get the center point
     * <p>
     * The y coordinate does not necessarily mean center on y. The y coordinate is ignored completely
     *
     * @return Vector coords
     */
    public Vector getCenter() {
        return center;
    }

    /**
     * Set the center point
     * <p>
     * The y coordinate will be ignored completely
     *
     * @param center Center vector coords
     * @return This cylinder shape
     */
    public Cylinder setCenter(Vector center) {
        this.center = center;
        return this;
    }

    /**
     * Get the radius of the x axis
     *
     * @return Radius of x axis
     */
    public double getRadiusX() {
        return radiusX;
    }

    /**
     * Set the radius of the x axis
     *
     * @param radiusX New x axis radius
     * @return This cylinder shape
     */
    public Cylinder setRadiusX(double radiusX) {
        this.radiusX = radiusX;
        return this;
    }

    /**
     * Get the radius of the z axis
     *
     * @return Radius of z axis
     */
    public double getRadiusZ() {
        return radiusZ;
    }

    /**
     * Set the radius of the z axis
     *
     * @param radiusZ New z axis radius
     * @return This cylinder shape
     */
    public Cylinder setRadiusZ(double radiusZ) {
        this.radiusZ = radiusZ;
        return this;
    }

    /**
     * Get the minimum y coordinate
     *
     * @return Minimum y coordinate
     */
    public int getMinY() {
        return minY;
    }

    /**
     * Set the minimum y coordinate
     *
     * @param minY New minimum y coordinate
     * @return This cylinder shape
     */
    public Cylinder setMinY(int minY) {
        this.minY = minY;
        return this;
    }

    /**
     * Get the maximum y coordinate
     *
     * @return Maximum y coordinate
     */
    public int getMaxY() {
        return maxY;
    }

    /**
     * Set the maximum y coordinate
     *
     * @param maxY New maximum y coordinate
     * @return This cylinder shape
     */
    public Cylinder setMaxY(int maxY) {
        this.maxY = maxY;
        return this;
    }

    /**
     * Clears the data for this shape
     */
    @Override
    public void clear() {
        center = null;
        radiusX = 0;
        radiusZ = 0;
        minY = 0;
        maxY = 0;
    }

    /**
     * Get a copy of this shape
     *
     * @return Copy of this shape
     */
    @Override
    public Cylinder clone() {
        try {
            return (Cylinder) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error(e);
        }
    }

    /**
     * Get a human readable String representation of this shape
     *
     * @return Human readable String representation of shape
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(256);
        sb.append("Cylinder[");
        if (center == null) {
            sb.append("null");
        } else {
            sb.append("center:[")
                    .append(center.getX())
                    .append(",")
                    .append(center.getY())
                    .append(",")
                    .append(center.getZ())
                    .append("], radius:[")
                    .append(radiusX)
                    .append(",")
                    .append(radiusZ)
                    .append("], minY:")
                    .append(minY)
                    .append(", maxY:")
                    .append(maxY)
                    .append("]");
        }
        return sb.append("]").toString();
    }
}
