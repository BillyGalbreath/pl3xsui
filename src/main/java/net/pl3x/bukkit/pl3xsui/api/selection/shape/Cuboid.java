package net.pl3x.bukkit.pl3xsui.api.selection.shape;

import net.pl3x.bukkit.pl3xsui.api.selection.Selection;
import org.bukkit.util.Vector;

/**
 * Represents a cuboid shape
 */
@SuppressWarnings("UnusedReturnValue")
public class Cuboid implements Selection, Cloneable {
    private Vector primary;
    private Vector secondary;

    /**
     * Get the primary point.
     * <p>
     * This is usually the first point selected, or in case of WorldEdit the left click with wand.
     *
     * @return Vector containing x,y,z coords
     */
    public Vector getPrimary() {
        return primary;
    }

    /**
     * Set the primary point.
     * <p>
     * This is usually the first point selected, or in case of WorldEdit the left click with wand.
     *
     * @param primary Vector containing x,y,z coords
     * @return This cuboid shape
     */
    public Cuboid setPrimary(Vector primary) {
        this.primary = primary;
        return this;
    }

    /**
     * Get the secondary point.
     * <p>
     * This is usually the second point selected, or in case of WorldEdit the right click with wand.
     *
     * @return Vector containing x,y,z coords
     */
    public Vector getSecondary() {
        return secondary;
    }

    /**
     * Set the secondary point.
     * <p>
     * This is usually the second point selected, or in case of WorldEdit the right click with wand.
     *
     * @param secondary Vector containing x,y,z coords
     * @return This cuboid shape
     */
    public Cuboid setSecondary(Vector secondary) {
        this.secondary = secondary;
        return this;
    }

    /**
     * Clears the data for this shape
     */
    @Override
    public void clear() {
        primary = null;
        secondary = null;
    }

    /**
     * Get a copy of this shape
     *
     * @return Copy of this shape
     */
    @Override
    public Cuboid clone() {
        try {
            return (Cuboid) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error(e);
        }
    }

    /**
     * Get a human readable String representation of this shape
     *
     * @return Human readable String representation of shape
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(256);
        sb.append("Cuboid[");
        if (primary == null && secondary == null) {
            sb.append("null");
        } else {
            sb.append("primary:[");
            if (primary == null) {
                sb.append("null");
            } else {
                sb.append(primary.getBlockX());
                sb.append(",");
                sb.append(primary.getBlockY());
                sb.append(",");
                sb.append(primary.getBlockZ());
            }
            sb.append("], secondary:[");
            if (secondary == null) {
                sb.append("null");
            } else {
                sb.append(secondary.getBlockX());
                sb.append(",");
                sb.append(secondary.getBlockY());
                sb.append(",");
                sb.append(secondary.getBlockZ());
            }
            sb.append("]");
        }
        return sb.append("]").toString();
    }
}
