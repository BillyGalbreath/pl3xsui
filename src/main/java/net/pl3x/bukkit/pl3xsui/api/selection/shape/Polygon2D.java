package net.pl3x.bukkit.pl3xsui.api.selection.shape;

import net.pl3x.bukkit.pl3xsui.api.selection.Selection;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a polygon2d shape
 */
@SuppressWarnings({"UnusedReturnValue", "unused"})
public class Polygon2D implements Selection, Cloneable {
    private List<Vector> points = new ArrayList<>();
    private int minY;
    private int maxY;

    /**
     * Get list of points
     *
     * @return List of vector coordinates
     */
    public List<Vector> getPoints() {
        return points;
    }

    /**
     * Set a new list of points
     *
     * @param points List of points
     * @return This polygon2d shape
     */
    public Polygon2D setPoints(List<Vector> points) {
        this.points = points;
        return this;
    }

    /**
     * Add a point at specific index
     * <p>
     * If the index position is greater that the size of the list of points the addition will fail
     *
     * @param index Index position
     * @param point Point to add
     * @return This polygon2d shape. Will return null if the addition failed
     */
    public Polygon2D addPoint(int index, Vector point) {
        if (points.size() > index) {
            return null;
        }
        points.add(point);
        return this;
    }

    /**
     * Get a point
     *
     * @param index Index position of point to get
     * @return Vector coordinates of point
     */
    public Vector getPoint(int index) {
        return points == null || points.size() <= index ? null : points.get(index);
    }

    /**
     * Get the minimum y coordinate
     *
     * @return The minimum y coordinate
     */
    public int getMinY() {
        return minY;
    }

    /**
     * Set the minimum y coordinate
     *
     * @param minY Minimum y coordinate
     * @return This polygon2d shape
     */
    public Polygon2D setMinY(int minY) {
        this.minY = minY;
        return this;
    }

    /**
     * Get the maximum y coordinate
     *
     * @return The maximum y coordinate
     */
    public int getMaxY() {
        return maxY;
    }

    /**
     * Set the maximum y coordinate
     *
     * @param maxY Maximum y coordinate
     * @return This polygon2d shape
     */
    public Polygon2D setMaxY(int maxY) {
        this.maxY = maxY;
        return this;
    }

    /**
     * Clears the data for this shape
     */
    @Override
    public void clear() {
        points.clear();
    }

    /**
     * Get a copy of this shape
     *
     * @return Copy of this shape
     */
    @Override
    public Polygon2D clone() {
        try {
            return (Polygon2D) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error(e);
        }
    }

    /**
     * Get a human readable String representation of this shape
     *
     * @return Human readable String representation of shape
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(256);
        sb.append("Polygon2D[");
        if (points == null || points.isEmpty()) {
            sb.append("null");
        } else {
            for (int i = 0; i < points.size(); i++) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append("p");
                sb.append(i);
                sb.append(":[");
                Vector point = points.get(i);
                if (point == null) {
                    sb.append("null");
                } else {
                    sb.append(point.getX());
                    sb.append(",");
                    sb.append(point.getY());
                    sb.append(",");
                    sb.append(point.getZ());
                }
                sb.append("]");
            }
            sb.append(", minY: ");
            sb.append(minY);
            sb.append(", maxY: ");
            sb.append(maxY);
        }
        return sb.append("]").toString();
    }
}
