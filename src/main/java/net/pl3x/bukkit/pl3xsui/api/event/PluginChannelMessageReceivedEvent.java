package net.pl3x.bukkit.pl3xsui.api.event;

import org.bukkit.entity.Player;

/**
 * Event triggered when server receives a message from a player on a plugin channel
 */
public class PluginChannelMessageReceivedEvent extends SelectionEvent {
    private final String channel;
    private final String message;

    /**
     * Create new received message event.
     * <p>
     * You should not create/fake these unless you know what you're doing.
     *
     * @param player  Player that sent the message to server
     * @param channel Channel the message was received on
     * @param message The message received
     */
    public PluginChannelMessageReceivedEvent(Player player, String channel, String message) {
        super(player);
        this.channel = channel;
        this.message = message;
    }

    /**
     * Get the channel the message was received on
     *
     * @return The channel name
     */
    public String getchannel() {
        return channel;
    }

    /**
     * Get the message that was received
     *
     * @return The message received
     */
    public String getMessage() {
        return message;
    }
}
