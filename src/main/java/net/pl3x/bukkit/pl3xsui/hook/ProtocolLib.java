package net.pl3x.bukkit.pl3xsui.hook;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import net.pl3x.bukkit.pl3xsui.Pl3xSUI;
import net.pl3x.bukkit.pl3xsui.api.PluginChannel;
import net.pl3x.bukkit.pl3xsui.api.event.PluginChannelMessageReceivedEvent;
import net.pl3x.bukkit.pl3xsui.api.event.PluginChannelMessageSentEvent;
import org.bukkit.Bukkit;

public class ProtocolLib {
    private static ProtocolManager protocolManager;
    private static boolean alreadyRegistered = false;

    public static ProtocolManager getManager() {
        if (protocolManager == null) {
            protocolManager = ProtocolLibrary.getProtocolManager();
        }
        return protocolManager;
    }

    public static void registerListeners(Pl3xSUI plugin) {
        if (alreadyRegistered) {
            throw new AssertionError();
        }

        final PluginChannel channelHandler = plugin.getPluginChannelHandler();

        getManager().addPacketListener(new PacketAdapter(plugin, PacketType.Play.Server.CUSTOM_PAYLOAD) {
            @Override
            public void onPacketSending(PacketEvent event) {
                PacketContainer packet = event.getPacket();
                PluginChannelMessageSentEvent sentEvent = new PluginChannelMessageSentEvent(event.getPlayer(),
                        packet.getStrings().read(0),
                        channelHandler.getPacketContents(packet));
                Bukkit.getServer().getPluginManager().callEvent(sentEvent);
            }
        });

        getManager().addPacketListener(new PacketAdapter(plugin, PacketType.Play.Client.CUSTOM_PAYLOAD) {
            @Override
            public void onPacketReceiving(PacketEvent event) {
                PacketContainer packet = event.getPacket();
                PluginChannelMessageReceivedEvent receivedEvent = new PluginChannelMessageReceivedEvent(event.getPlayer(),
                        packet.getStrings().read(0),
                        channelHandler.getPacketContents(packet));
                Bukkit.getServer().getPluginManager().callEvent(receivedEvent);
            }
        });

        alreadyRegistered = true;
    }

    public static void unregisterListeners(Pl3xSUI plugin) {
        protocolManager.removePacketListeners(plugin);
        alreadyRegistered = false;
    }
}
