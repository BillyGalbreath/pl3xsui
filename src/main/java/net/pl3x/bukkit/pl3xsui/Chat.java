package net.pl3x.bukkit.pl3xsui;

import net.pl3x.bukkit.pl3xsui.configuration.Lang;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Chat object to format and send chat text to a CommandSender easily
 */
public class Chat {
    private String message;

    /**
     * Create new chat message
     *
     * @param lang Lang message
     */
    public Chat(Lang lang) {
        this(lang.toString());
    }

    /**
     * Create new chat message
     *
     * @param message String message
     */
    public Chat(String message) {
        this.message = message;

        colorize(); // auto color new chat messages
    }

    /**
     * Send message to CommandSender
     * <p>
     * Messages containing \n will be split into multiple messages
     *
     * @param recipient CommandSender
     */
    public void send(CommandSender recipient) {
        if (message == null || ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\\n")) {
            recipient.sendMessage(part);
        }
    }

    private void colorize() {
        message = ChatColor.translateAlternateColorCodes('&', message);
    }
}
