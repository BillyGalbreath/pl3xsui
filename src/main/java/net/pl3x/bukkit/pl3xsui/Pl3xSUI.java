package net.pl3x.bukkit.pl3xsui;

import net.pl3x.bukkit.pl3xsui.api.PluginChannel;
import net.pl3x.bukkit.pl3xsui.command.CmdPl3xSUI;
import net.pl3x.bukkit.pl3xsui.configuration.ColorConfig;
import net.pl3x.bukkit.pl3xsui.configuration.Lang;
import net.pl3x.bukkit.pl3xsui.hook.ProtocolLib;
import net.pl3x.bukkit.pl3xsui.listener.GriefPreventionVisualsListener;
import net.pl3x.bukkit.pl3xsui.listener.PlayerListener;
import net.pl3x.bukkit.pl3xsui.listener.PluginChannelListener;
import net.pl3x.bukkit.pl3xsui.listener.SelectionListener;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The Pl3xSUI main class
 */
public class Pl3xSUI extends JavaPlugin {
    private PluginChannel pluginChannelHandler;
    private GriefPreventionVisualsListener gpListener;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        Lang.reload();

        ColorConfig.saveDefaultConfig();

        if (getServer().getPluginManager().isPluginEnabled("GriefPrevention")) {
            Logger.info("GriefPrevention found. Hooking into claim visuals.");
            gpListener = new GriefPreventionVisualsListener(this);
            getServer().getPluginManager().registerEvents(gpListener, this);
        }

        ColorConfig.loadColors();

        // register bukkit event listeners
        getServer().getPluginManager().registerEvents(new PluginChannelListener(this), this);
        getServer().getPluginManager().registerEvents(new SelectionListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerListener(), this);

        // register protocollib packet listeners
        ProtocolLib.registerListeners(this);

        // register commands
        getCommand("pl3xsui").setExecutor(new CmdPl3xSUI(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        if (gpListener != null) {
            gpListener.cancelAll();
        }

        Pl3xPlayer.unloadAll();
        ProtocolLib.unregisterListeners(this);

        Logger.info(getName() + " disabled.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        sender.sendMessage(ChatColor.DARK_RED + getName() + " is disabled. Please check console logs for more information.");
        return true;
    }

    /**
     * Get the plugin channel handler
     *
     * @return PluginChannel handler
     */
    public PluginChannel getPluginChannelHandler() {
        if (pluginChannelHandler == null) {
            pluginChannelHandler = new PluginChannelHandler();
        }
        return pluginChannelHandler;
    }
}
