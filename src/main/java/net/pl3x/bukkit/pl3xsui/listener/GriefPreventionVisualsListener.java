package net.pl3x.bukkit.pl3xsui.listener;

import me.ryanhamshire.GriefPrevention.Claim;
import me.ryanhamshire.GriefPrevention.events.VisualizationEvent;
import net.pl3x.bukkit.pl3xsui.Logger;
import net.pl3x.bukkit.pl3xsui.Pl3xSUI;
import net.pl3x.bukkit.pl3xsui.api.ParticleColor;
import net.pl3x.bukkit.pl3xsui.api.selection.shape.Cuboid;
import net.pl3x.bukkit.pl3xsui.configuration.Config;
import net.pl3x.bukkit.pl3xsui.task.CuboidParticlesTask;
import net.pl3x.bukkit.pl3xsui.task.ParticlesTask;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * GriefPrevention Hook
 */
public class GriefPreventionVisualsListener implements Listener {
    private final Pl3xSUI plugin;
    private final Map<UUID, Set<ParticlesTask>> visualTasks = new HashMap<>();
    private final Map<UUID, Timeout> timeouts = new HashMap<>();

    public GriefPreventionVisualsListener(Pl3xSUI plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onVisualizeClaims(VisualizationEvent event) {
        if (!Config.GRIEFPREVENTION_VISUALS.getBoolean()) {
            return; // gp visuals disabled in config
        }
        Logger.debug("GP Visualization Event Received");

        Player player = event.getPlayer();
        UUID uuid = player.getUniqueId();

        // cancel previous visual tasks
        if (visualTasks.containsKey(uuid)) {
            visualTasks.get(uuid).forEach(ParticlesTask::cancel);
        }
        if (timeouts.containsKey(uuid)) {
            timeouts.get(uuid).cancel();
        }

        // get all the claims and subdivide claims we need to show
        Set<Claim> claims = new HashSet<>();
        claims.addAll(event.getClaims());
        if (event.showSubdivides()) {
            for (Claim claim : event.getClaims()) {
                if (claim.children != null && !claim.children.isEmpty()) {
                    claims.addAll(claim.children);
                }
                if (claim.parent != null) {
                    claims.add(claim.parent);
                    claims.addAll(claim.parent.children);
                }
            }
        }

        // start new tasks
        Set<ParticlesTask> tasks = new HashSet<>();
        for (Claim claim : claims) {
            Cuboid cuboid = new Cuboid();
            cuboid.setPrimary(claim.getGreaterBoundaryCorner().toVector());
            cuboid.setSecondary(claim.getLesserBoundaryCorner().toVector());

            cuboid.getPrimary().setY(Math.min(cuboid.getPrimary().getY(), cuboid.getSecondary().getY()));
            cuboid.getSecondary().setY(256);

            CuboidParticlesTask task = new CuboidParticlesTask(player, cuboid);
            task.setFillColor(claim.parent != null ? ParticleColor.getColor("#fefefe") : claim.isAdminClaim() ? ParticleColor.getColor("#ffa500") : ParticleColor.getColor("#ffff00"));
            task.setEdgeColor(ParticleColor.getColor("#00ff00"));

            task.runTaskTimer(Pl3xSUI.getPlugin(Pl3xSUI.class), 5, Config.PARTICLE_DELAY.getInt());
            tasks.add(task);
        }
        visualTasks.put(uuid, tasks);

        Timeout timeout = new Timeout(uuid);
        timeout.runTaskLater(plugin, 60 * 20);
        timeouts.put(uuid, timeout);
    }

    public void cancelAll() {
        for (Set<ParticlesTask> tasks : visualTasks.values()) {
            tasks.forEach(ParticlesTask::cancel);
        }
        timeouts.values().forEach(BukkitRunnable::cancel);
    }

    private class Timeout extends BukkitRunnable {
        private final UUID uuid;

        Timeout(UUID uuid) {
            this.uuid = uuid;
        }

        @Override
        public void run() {
            if (visualTasks.containsKey(uuid)) {
                visualTasks.get(uuid).forEach(ParticlesTask::cancel);
            }
        }
    }
}
