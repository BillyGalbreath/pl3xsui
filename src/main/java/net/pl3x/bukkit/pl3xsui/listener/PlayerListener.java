package net.pl3x.bukkit.pl3xsui.listener;

import net.pl3x.bukkit.pl3xsui.Pl3xPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        Pl3xPlayer.getPlayers().stream()
                .filter(pl3xPlayer -> pl3xPlayer.isWatching(player))
                .forEach(pl3xPlayer -> pl3xPlayer.removeWatcher(player));
        Pl3xPlayer.remove(player);
    }
}
