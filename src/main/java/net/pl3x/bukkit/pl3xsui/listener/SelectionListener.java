package net.pl3x.bukkit.pl3xsui.listener;

import net.pl3x.bukkit.pl3xsui.Logger;
import net.pl3x.bukkit.pl3xsui.Pl3xPlayer;
import net.pl3x.bukkit.pl3xsui.api.event.CuboidSelectionEvent;
import net.pl3x.bukkit.pl3xsui.api.event.CylinderSelectionEvent;
import net.pl3x.bukkit.pl3xsui.api.event.EllipsoidSelectionEvent;
import net.pl3x.bukkit.pl3xsui.api.event.Polygon2DSelectionEvent;
import net.pl3x.bukkit.pl3xsui.api.event.PolyhedronSelectionEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class SelectionListener implements Listener {
    @EventHandler(priority = EventPriority.MONITOR)
    public void onCuboidSelection(CuboidSelectionEvent event) {
        if (event.isCancelled()) {
            Logger.debug("[CuboidSelectionEvent] Another plugin cancelled the event.");
            return;
        }
        Pl3xPlayer player = Pl3xPlayer.getPlayer(event.getPlayer());
        player.setSelection(event.getCuboid());
        player.showSelection();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onEllipsoidSelection(EllipsoidSelectionEvent event) {
        if (event.isCancelled()) {
            Logger.debug("[EllipsoidSelectionEvent] Another plugin cancelled the event.");
            return;
        }
        Pl3xPlayer player = Pl3xPlayer.getPlayer(event.getPlayer());
        player.setSelection(event.getEllipsoid());
        player.showSelection();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onCylinderSelection(CylinderSelectionEvent event) {
        if (event.isCancelled()) {
            Logger.debug("[CylinderSelectionEvent] Another plugin cancelled the event.");
            return;
        }
        Pl3xPlayer player = Pl3xPlayer.getPlayer(event.getPlayer());
        player.setSelection(event.getCylinder());
        player.showSelection();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPolygon2DSelection(Polygon2DSelectionEvent event) {
        if (event.isCancelled()) {
            Logger.debug("[Polygon2DSelectionEvent] Another plugin cancelled the event.");
            return;
        }
        Pl3xPlayer player = Pl3xPlayer.getPlayer(event.getPlayer());
        player.setSelection(event.getPolygon2D());
        player.showSelection();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPolyhedronSelection(PolyhedronSelectionEvent event) {
        if (event.isCancelled()) {
            Logger.debug("[PolyhedronSelectionEvent] Another plugin cancelled the event.");
            return;
        }
        Pl3xPlayer player = Pl3xPlayer.getPlayer(event.getPlayer());
        player.setSelection(event.getPolyhedron());
        player.showSelection();
    }
}
