package net.pl3x.bukkit.pl3xsui.listener;

import net.pl3x.bukkit.pl3xsui.Logger;
import net.pl3x.bukkit.pl3xsui.Pl3xPlayer;
import net.pl3x.bukkit.pl3xsui.Pl3xSUI;
import net.pl3x.bukkit.pl3xsui.api.event.CuboidSelectionEvent;
import net.pl3x.bukkit.pl3xsui.api.event.CylinderSelectionEvent;
import net.pl3x.bukkit.pl3xsui.api.event.EllipsoidSelectionEvent;
import net.pl3x.bukkit.pl3xsui.api.event.PluginChannelMessageReceivedEvent;
import net.pl3x.bukkit.pl3xsui.api.event.PluginChannelMessageSentEvent;
import net.pl3x.bukkit.pl3xsui.api.event.Polygon2DSelectionEvent;
import net.pl3x.bukkit.pl3xsui.api.event.PolyhedronSelectionEvent;
import net.pl3x.bukkit.pl3xsui.api.selection.Selection;
import net.pl3x.bukkit.pl3xsui.api.selection.shape.Cuboid;
import net.pl3x.bukkit.pl3xsui.api.selection.shape.Cylinder;
import net.pl3x.bukkit.pl3xsui.api.selection.shape.Ellipsoid;
import net.pl3x.bukkit.pl3xsui.api.selection.shape.Polygon2D;
import net.pl3x.bukkit.pl3xsui.api.selection.shape.Polyhedron;
import net.pl3x.bukkit.pl3xsui.configuration.Config;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.util.Vector;

import java.nio.charset.Charset;

public class PluginChannelListener implements Listener {
    private final Pl3xSUI plugin;
    private final String WECUI_CHANNEL = "WECUI";
    private final boolean hasWorldEdit;

    public PluginChannelListener(Pl3xSUI plugin) {
        this.plugin = plugin;
        hasWorldEdit = plugin.getServer().getPluginManager().isPluginEnabled("WorldEdit");
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        // register the client as having WECUI (even though they probably dont)
        plugin.getPluginChannelHandler().registerPlayerChannel(player, WECUI_CHANNEL);

        // send the WECUI version. for some reason the client mod sends it 3 times, so we will do the same.
        byte[] bytes = "v|3".getBytes(Charset.forName("UTF-8"));
        Bukkit.getMessenger().dispatchIncomingMessage(player, WECUI_CHANNEL, bytes);
        Bukkit.getMessenger().dispatchIncomingMessage(player, WECUI_CHANNEL, bytes);
        Bukkit.getMessenger().dispatchIncomingMessage(player, WECUI_CHANNEL, bytes);

        // tell worldedit we are listening for cui descriptions if WorldEdit is installed
        if (hasWorldEdit) {
            player.performCommand("we cui");
        }
    }

    @EventHandler
    public void onServerSentToClient(PluginChannelMessageSentEvent event) {
        if (!event.getchannel().equals(WECUI_CHANNEL)) {
            return; // only listen on WECUI channel
        }

        Player player = event.getPlayer();
        String rawMessage = event.getMessage();

        Logger.debug("&bServer &e-> &2" + player.getName() + "&e: &3" + rawMessage);

        String[] parts = rawMessage.split("\\|");
        if (parts.length == 0) {
            return; // blank message?
        }

        Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);

        // set shape
        // s|cuboid
        // s|ellipsoid
        // s|cylinder
        // s|polygon2d
        // s|polyhedron
        if (parts.length == 2 && parts[0].equals("s")) {
            switch (parts[1]) {
                case "cuboid": // cuboid
                    CuboidSelectionEvent cuboidSelectionEvent = new CuboidSelectionEvent(player, new Cuboid());
                    Bukkit.getServer().getPluginManager().callEvent(cuboidSelectionEvent);
                    return;
                case "ellipsoid": // ellipsoid & sphere
                    EllipsoidSelectionEvent ellipsoidSelectionEvent = new EllipsoidSelectionEvent(player, new Ellipsoid());
                    Bukkit.getServer().getPluginManager().callEvent(ellipsoidSelectionEvent);
                    return;
                case "cylinder": // cyl
                    CylinderSelectionEvent cylinderSelectionEvent = new CylinderSelectionEvent(player, new Cylinder());
                    Bukkit.getServer().getPluginManager().callEvent(cylinderSelectionEvent);
                    return;
                case "polygon2d": // poly
                    Polygon2DSelectionEvent polygon2DSelectionEvent = new Polygon2DSelectionEvent(player, new Polygon2D());
                    Bukkit.getServer().getPluginManager().callEvent(polygon2DSelectionEvent);
                    return;
                case "polyhedron": // convex
                    PolyhedronSelectionEvent polyhedronSelectionEvent = new PolyhedronSelectionEvent(player, new Polyhedron());
                    Bukkit.getServer().getPluginManager().callEvent(polyhedronSelectionEvent);
                    return;
            }
            Logger.debug("Selection shape unknown: " + parts[1]);
            return;
        }

        Selection selection = pl3xPlayer.getSelection();
        if (selection != null) {
            selection = selection.clone(); // clone any existing selections so we dont alter them here
        }

        // cuboid point
        // p|0|0|0|0|0
        // p|t|x|y|z|v
        if (parts.length == 6 && parts[0].equals("p") && selection instanceof Cuboid) {
            final int t, x, y, z;
            try {
                t = Integer.valueOf(parts[1]); // type (primary/secondary)
                x = Integer.valueOf(parts[2]); // x coord
                y = Integer.valueOf(parts[3]); // y coord
                z = Integer.valueOf(parts[4]); // z coord
            } catch (NumberFormatException e) {
                Logger.warn("Invalid cuboid data from WorldEdit: " + rawMessage);
                return;
            }

            Cuboid cuboid = (Cuboid) selection;

            if (t == 0) {
                cuboid.setPrimary(new Vector(x, y, z));
            } else if (t == 1) {
                cuboid.setSecondary(new Vector(x, y, z));
            } else {
                Logger.warn("Invalid cuboid data from WorldEdit: " + rawMessage);
                return;
            }

            CuboidSelectionEvent cuboidSelectionEvent = new CuboidSelectionEvent(player, cuboid);
            Bukkit.getServer().getPluginManager().callEvent(cuboidSelectionEvent);
            return;
        }

        // ellipsoid point (colors spheres too)
        // e|0|0|0|0
        // e|t|x|y|z
        if (parts.length == 5 && parts[0].equals("e")) {
            final int t, x, y, z;
            try {
                t = Integer.valueOf(parts[1]); // type (center/radius)
                x = Integer.valueOf(parts[2]); // x coord
                y = Integer.valueOf(parts[3]); // y coord
                z = Integer.valueOf(parts[4]); // z coord
            } catch (NumberFormatException e) {
                Logger.warn("Invalid ellipsoid data from WorldEdit: " + rawMessage);
                return;
            }

            Ellipsoid ellipsoid = (selection instanceof Ellipsoid) ? ((Ellipsoid) selection) : new Ellipsoid();

            if (t == 0) {
                ellipsoid.setCenter(new Vector(x, y, z));
            } else if (t == 1) {
                ellipsoid.setRadius(new Vector(x, y, z));
            } else {
                Logger.warn("Invalid ellipsoid data from WorldEdit: " + rawMessage);
                return;
            }

            EllipsoidSelectionEvent ellipsoidSelectionEvent = new EllipsoidSelectionEvent(player, ellipsoid);
            Bukkit.getServer().getPluginManager().callEvent(ellipsoidSelectionEvent);
            return;
        }

        // cylinder center
        // cyl|0|0|0|0.0|0.0
        // cyl|x|y|z|rX|rZ
        if (parts.length == 6 && parts[0].equals("cyl")) {
            Cylinder cylinder;
            try {
                cylinder = ((selection instanceof Cylinder) ? ((Cylinder) selection) : new Cylinder())
                        .setCenter(new Vector(
                                Integer.valueOf(parts[1]) + 0.5D,
                                Integer.valueOf(parts[2]),
                                Integer.valueOf(parts[3]) + 0.5D))
                        .setRadiusX(Double.valueOf(parts[4]))
                        .setRadiusZ(Double.valueOf(parts[5]))
                        .setMinY(0)
                        .setMaxY(0);
            } catch (NumberFormatException e) {
                Logger.warn("Invalid cylinder data from WorldEdit: " + rawMessage);
                return;
            }

            CylinderSelectionEvent cylinderSelectionEvent = new CylinderSelectionEvent(player, cylinder);
            Bukkit.getServer().getPluginManager().callEvent(cylinderSelectionEvent);
            return;
        }

        // polygon2d point
        // p2|0|0|0|0
        // p2|#|x|z|v
        if (parts.length == 5 && parts[0].equals("p2")) {
            final int i, x, z;
            try {
                i = Integer.valueOf(parts[1]); // index
                x = Integer.valueOf(parts[2]); // x coord
                z = Integer.valueOf(parts[3]); // z coord
            } catch (NumberFormatException e) {
                Logger.warn("Invalid polygon2d data from WorldEdit: " + rawMessage);
                return;
            }

            Polygon2D polygon2d = (selection instanceof Polygon2D) ? ((Polygon2D) selection) : new Polygon2D();
            int size = polygon2d.getPoints().size();
            if (size < i) {
                Logger.warn("Invalid polygon2d index from WorldEdit: " + i + ">" + size);
                return;
            }

            polygon2d.addPoint(i, new Vector(x + 0.5D, 0.0D, z + 0.5D));

            Polygon2DSelectionEvent polygon2dSelectionEvent = new Polygon2DSelectionEvent(player, polygon2d);
            Bukkit.getServer().getPluginManager().callEvent(polygon2dSelectionEvent);
            return;
        }

        // cylinder and polygon min and max coords for Y
        // mm|0|0
        // mm|minY|maxY
        if (parts.length == 3 && parts[0].equals("mm")) {
            int minY, maxY;
            try {
                minY = Integer.valueOf(parts[1]);
                maxY = Integer.valueOf(parts[2]);
            } catch (NumberFormatException e) {
                Logger.warn("Invalid y-coord data from WorldEdit: " + rawMessage);
                return;
            }

            Event yCoordEvent;
            if (selection instanceof Cylinder) {
                yCoordEvent = new CylinderSelectionEvent(player, ((Cylinder) selection).setMinY(minY).setMaxY(maxY));
            } else if (selection instanceof Polygon2D) {
                yCoordEvent = new Polygon2DSelectionEvent(player, ((Polygon2D) selection).setMinY(minY).setMaxY(maxY));
            } else {
                Logger.warn("Invalid y-coord data from WorldEdit: " + rawMessage);
                return;
            }

            Bukkit.getServer().getPluginManager().callEvent(yCoordEvent);
            return;
        }

        // convex (polyhedron) point
        // p|0|0|0|0|0
        // p|#|x|y|z|v
        if (parts.length == 6 && parts[0].equals("p")) {
            final int i, x, y, z;
            try {
                i = Integer.valueOf(parts[1]); // index
                x = Integer.valueOf(parts[2]); // x coord
                y = Integer.valueOf(parts[3]); // y coord
                z = Integer.valueOf(parts[4]); // z coord
            } catch (NumberFormatException e) {
                Logger.warn("Invalid polyhedron data from WorldEdit: " + rawMessage);
                return;
            }

            Polyhedron polyhedron = (selection instanceof Polyhedron) ? ((Polyhedron) selection) : new Polyhedron();
            if (i == 0 && Config.CLEANUP_CONVEX.getBoolean()) {
                polyhedron = new Polyhedron();
            }
            int size = polyhedron.getPoints().size();
            if (size < i) {
                Logger.warn("Invalid polyhedron index from WorldEdit: " + i + ">" + size);
                return;
            }

            polyhedron.addPoint(i, new Vector(x + 0.5D, y + 0.5D, z + 0.5D));

            PolyhedronSelectionEvent polyhedronSelectionEvent = new PolyhedronSelectionEvent(player, polyhedron);
            Bukkit.getServer().getPluginManager().callEvent(polyhedronSelectionEvent);
            return;
        }

        // convex (polyhedron) point
        // p|0|0|0
        // p|p1|p2|p3 (the 3 points to form a poly, aka triangle)
        if (parts.length == 4 && parts[0].equals("poly")) {
            final int p1, p2, p3;
            try {
                p1 = Integer.valueOf(parts[1]); // point 1
                p2 = Integer.valueOf(parts[2]); // point 2
                p3 = Integer.valueOf(parts[3]); // point 3
            } catch (NumberFormatException e) {
                Logger.warn("Invalid polyhedron data from WorldEdit: " + rawMessage);
                return;
            }

            Polyhedron polyhedron = (selection instanceof Polyhedron) ? ((Polyhedron) selection) : new Polyhedron();

            polyhedron.addFace(new Vector(p1, p2, p3));

            PolyhedronSelectionEvent polyhedronSelectionEvent = new PolyhedronSelectionEvent(player, polyhedron);
            Bukkit.getServer().getPluginManager().callEvent(polyhedronSelectionEvent);
            return;
        }

        Logger.debug("Unrecognized data received: " + rawMessage);
    }

    @EventHandler
    public void onServerReceivedFromClient(PluginChannelMessageReceivedEvent event) {
        if (!event.getchannel().equals(WECUI_CHANNEL)) {
            return; // only listen on WECUI channel
        }

        // Maybe disable Pl3xSUI for this user if they have the official WorldEditCUI client mod installed?

        Player player = event.getPlayer();
        String rawMessage = event.getMessage();

        Logger.debug("&2" + player.getName() + " &e-> &bServer&e: &3" + rawMessage);
    }
}
