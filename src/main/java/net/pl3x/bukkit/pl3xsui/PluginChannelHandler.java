package net.pl3x.bukkit.pl3xsui;

import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.reflect.StructureModifier;
import io.netty.buffer.ByteBuf;
import net.pl3x.bukkit.pl3xsui.api.PluginChannel;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

@SuppressWarnings("unused")
public class PluginChannelHandler implements PluginChannel {
    public void registerPlayerChannel(Player player, String channel) {
        ((CraftPlayer) player).addChannel(channel);
    }

    public String getPacketContents(PacketContainer packet) {
        StructureModifier<Object> data = packet.getModifier().withType(ByteBuf.class);
        if (data.size() == 0) {
            return null;
        }
        return new String(((ByteBuf) data.read(0)).array());
    }
}
