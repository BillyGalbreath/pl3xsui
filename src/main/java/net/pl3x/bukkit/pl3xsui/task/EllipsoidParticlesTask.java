package net.pl3x.bukkit.pl3xsui.task;

import net.pl3x.bukkit.pl3xsui.api.selection.shape.Ellipsoid;
import net.pl3x.bukkit.pl3xsui.configuration.PlayerConfig;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("WeakerAccess")
public class EllipsoidParticlesTask extends ParticlesTask {
    private final Vector center;
    private Vector radius;

    public EllipsoidParticlesTask(Player player, Ellipsoid selection) {
        super(player);

        center = selection.getCenter();

        radius = selection.getRadius();
        if (radius == null) {
            radius = new Vector(0, 0, 0);
        }

        if (radius.getX() == radius.getY() && radius.getX() == radius.getZ()) {
            setEdgeColor(PlayerConfig.COLOR_SPHERE_EDGE.getColor(player.getUniqueId()));
            setFillColor(PlayerConfig.COLOR_SPHERE_FILL.getColor(player.getUniqueId()));
        } else {
            setEdgeColor(PlayerConfig.COLOR_ELLIPSOID_EDGE.getColor(player.getUniqueId()));
            setFillColor(PlayerConfig.COLOR_ELLIPSOID_FILL.getColor(player.getUniqueId()));
        }
    }

    @Override
    public void run() {
        calculatePoints();

        super.run();
    }

    public void calculatePoints() {
        if (hasAlreadyCalculated()) {
            return;
        }
        setAlreadyCalculated(true);

        if (center == null) {
            setCancelled(true);
            return;
        }

        Set<Vector> vectors = new HashSet<>();
        Vector radiusTemp = new Vector(radius.getX() + 0.5D, radius.getY() + 0.5D, radius.getZ() + 0.5D);
        double width = radiusTemp.getX() * 2;
        double height = radiusTemp.getY() * 2;
        double length = radiusTemp.getZ() * 2;
        double minX = center.getBlockX() - radiusTemp.getX();
        double minY = center.getBlockY() - radiusTemp.getY();
        double minZ = center.getBlockZ() - radiusTemp.getZ();
        Vector center = new Vector(minX + width / 2.0D + 0.5D, minY + height / 2.0D + 0.5D, minZ + length / 2.0D + 0.5D);

        vectors.addAll(calcEllipse(center, new Vector(0.0D, radiusTemp.getY(), radiusTemp.getZ())));
        vectors.addAll(calcEllipse(center, new Vector(radiusTemp.getX(), 0.0D, radiusTemp.getZ())));
        vectors.addAll(calcEllipse(center, new Vector(radiusTemp.getX(), radiusTemp.getY(), 0.0D)));
        for (Vector vector : vectors) {
            addPacket(vector, getEdgeColor());
        }

        vectors.clear();
        for (double offset = 1; offset < radiusTemp.getY(); offset++) {
            Vector center1 = new Vector(center.getX(), center.getY() - offset, center.getZ());
            Vector center2 = new Vector(center.getX(), center.getY() + offset, center.getZ());
            double difference = Math.abs(center1.getY() - center.getY());
            double radiusRatio = Math.cos(Math.asin(difference / radiusTemp.getY()));
            double rx = radiusTemp.getX() * radiusRatio;
            double rz = radiusTemp.getZ() * radiusRatio;
            vectors.addAll(calcEllipse(center1, new Vector(rx, 0.0D, rz)));
            vectors.addAll(calcEllipse(center2, new Vector(rx, 0.0D, rz)));
        }
        for (Vector vector : vectors) {
            addPacket(vector, getFillColor());
        }
    }

    private Set<Vector> calcEllipse(Vector center, Vector radius) {
        Set<Vector> vectors = new HashSet<>();
        double biggestR = Math.max(radius.getX(), Math.max(radius.getY(), radius.getZ()));
        double circleCircumference = 2.0D * biggestR * Math.PI;
        double deltaTheta = 1.0D / circleCircumference;
        double twoPi = 2.0D * Math.PI;
        for (double i = 0.0D; i < 1.0D; i += deltaTheta) {
            double x = center.getX();
            double y = center.getY();
            double z = center.getZ();
            if (radius.getX() == 0.0D) {
                y = center.getY() + Math.cos(i * twoPi) * radius.getY();
                z = center.getZ() + Math.sin(i * twoPi) * radius.getZ();
            } else if (radius.getY() == 0.0D) {
                x = center.getX() + Math.cos(i * twoPi) * radius.getX();
                z = center.getZ() + Math.sin(i * twoPi) * radius.getZ();
            } else if (radius.getZ() == 0.0D) {
                x = center.getX() + Math.cos(i * twoPi) * radius.getX();
                y = center.getY() + Math.sin(i * twoPi) * radius.getY();
            }
            Vector loc = new Vector(x, y, z);
            vectors.add(loc);
        }
        return vectors;
    }
}
