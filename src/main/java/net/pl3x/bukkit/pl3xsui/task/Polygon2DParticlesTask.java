package net.pl3x.bukkit.pl3xsui.task;

import net.pl3x.bukkit.pl3xsui.api.selection.shape.Polygon2D;
import net.pl3x.bukkit.pl3xsui.configuration.PlayerConfig;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SuppressWarnings("WeakerAccess")
public class Polygon2DParticlesTask extends ParticlesTask {
    private final List<Vector> points;
    private final int minY;
    private final int maxY;

    public Polygon2DParticlesTask(Player player, Polygon2D selection) {
        super(player);
        this.points = selection.getPoints();
        this.minY = selection.getMinY();
        this.maxY = selection.getMaxY() + 1;

        setEdgeColor(PlayerConfig.COLOR_POLYGON2D_EDGE.getColor(player.getUniqueId()));
        setEdgeColor(PlayerConfig.COLOR_POLYGON2D_FILL.getColor(player.getUniqueId()));
    }

    @Override
    public void run() {
        calculatePoints();

        super.run();
    }

    public void calculatePoints() {
        if (hasAlreadyCalculated()) {
            return;
        }
        setAlreadyCalculated(true);

        if (points == null || points.isEmpty()) {
            setCancelled(true);
            return;
        }

        for (int i = 0; i < points.size(); i++) {
            Vector point = points.get(i);

            // draw vertical post
            for (Vector vector : calcPost(point)) {
                addPacket(vector, getEdgeColor());
            }

            // check if finished and wrap back to first post
            if (i == points.size() - 1) {
                // check if this is already first post
                if (i == 0) {
                    break; // done. do not draw line to self
                }

                // draw horizontal line back to first post
                for (Vector vector : calcLines(point, points.get(0))) {
                    addPacket(vector, getFillColor());
                }
                break; // done. nothing left to process.
            }

            // draw horizontal line to next point
            for (Vector vector : calcLines(point, points.get(i + 1))) {
                addPacket(vector, getFillColor());
            }
        }
    }

    private Set<Vector> calcPost(Vector point) {
        Set<Vector> lines = new HashSet<>();
        lines.addAll(calcLine(new Vector(point.getX() + 0.5D, minY, point.getZ() + 0.5D), new Vector(point.getX() + 0.5D, maxY, point.getZ() + 0.5D)));
        lines.addAll(calcLine(new Vector(point.getX() + 0.5D, minY, point.getZ() - 0.5D), new Vector(point.getX() + 0.5D, maxY, point.getZ() - 0.5D)));
        lines.addAll(calcLine(new Vector(point.getX() - 0.5D, minY, point.getZ() + 0.5D), new Vector(point.getX() - 0.5D, maxY, point.getZ() + 0.5D)));
        lines.addAll(calcLine(new Vector(point.getX() - 0.5D, minY, point.getZ() - 0.5D), new Vector(point.getX() - 0.5D, maxY, point.getZ() - 0.5D)));
        return lines;
    }

    private Set<Vector> calcLines(Vector point1, Vector point2) {
        Set<Vector> lines = new HashSet<>();
        for (int i = minY; i <= maxY; i++) {
            lines.addAll(calcLine(new Vector(point1.getX(), i, point1.getZ()), new Vector(point2.getX(), i, point2.getZ())));
        }
        return lines;
    }

    private Set<Vector> calcLine(Vector vec1, Vector vec2) {
        Vector p1 = vec1.clone();
        Vector p2 = vec2.clone();
        Set<Vector> vectors = new HashSet<>();
        double length = p1.distance(p2);
        int points = (int) (length / 1.0D) + 1;
        double gap = length / (points - 1);
        Vector gapVector = p2.clone().subtract(p1).normalize().multiply(gap);
        for (int i = 0; i < points; i++) {
            Vector currentPoint = p1.clone().add(gapVector.clone().multiply(i));
            vectors.add(currentPoint);
        }
        return vectors;
    }
}
