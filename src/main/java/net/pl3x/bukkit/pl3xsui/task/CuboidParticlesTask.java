package net.pl3x.bukkit.pl3xsui.task;

import net.pl3x.bukkit.pl3xsui.api.ParticleColor;
import net.pl3x.bukkit.pl3xsui.api.selection.shape.Cuboid;
import net.pl3x.bukkit.pl3xsui.configuration.PlayerConfig;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

@SuppressWarnings("WeakerAccess")
public class CuboidParticlesTask extends ParticlesTask {
    private Vector primary;
    private Vector secondary;

    public CuboidParticlesTask(Player player, Cuboid selection) {
        super(player);
        this.primary = selection.getPrimary();
        this.secondary = selection.getSecondary();

        setEdgeColor(PlayerConfig.COLOR_CUBOID_EDGE.getColor(player.getUniqueId()));
        setFillColor(PlayerConfig.COLOR_CUBOID_FILL.getColor(player.getUniqueId()));
    }

    @Override
    public void run() {
        calculatePoints();

        super.run();
    }

    public void calculatePoints() {
        if (hasAlreadyCalculated()) {
            return;
        }
        setAlreadyCalculated(true);

        if (primary == null && secondary == null) {
            setCancelled(true);
            return;
        }

        if (primary == null) {
            primary = secondary.clone();
        }

        if (secondary == null) {
            secondary = primary.clone();
        }

        double minX = Math.min(primary.getX(), secondary.getX());
        double minY = Math.min(primary.getY(), secondary.getY());
        double minZ = Math.min(primary.getZ(), secondary.getZ());
        double maxX = Math.max(primary.getX(), secondary.getX()) + 1; // add 1 to compensate for block size
        double maxY = Math.max(primary.getY(), secondary.getY()) + 1;
        double maxZ = Math.max(primary.getZ(), secondary.getZ()) + 1;

        for (double y = minY; y <= maxY; y++) {
            for (double x = minX; x <= maxX; x++) {
                ParticleColor color = x == minX || x == maxX || y == minY || y == maxY ? getEdgeColor() : getFillColor();
                addPacket(new Vector(x, y, minZ), color); // north
                addPacket(new Vector(x, y, maxZ), color); // south
            }
            for (double z = minZ + 1; z < maxZ; z++) {
                ParticleColor color = y == minY || y == maxY ? getEdgeColor() : getFillColor();
                addPacket(new Vector(minX, y, z), color); // west
                addPacket(new Vector(maxX, y, z), color); // east
                if (y == minY || y == maxY) {
                    for (double x = minX + 1; x < maxX; x++) {
                        addPacket(new Vector(x, y, z), getFillColor()); // top/bottom
                    }
                }
            }
        }
    }
}
