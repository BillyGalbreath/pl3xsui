package net.pl3x.bukkit.pl3xsui.task;

import net.pl3x.bukkit.pl3xsui.api.ParticleColor;
import net.pl3x.bukkit.pl3xsui.api.selection.shape.Cylinder;
import net.pl3x.bukkit.pl3xsui.configuration.PlayerConfig;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SuppressWarnings("WeakerAccess")
public class CylinderParticlesTask extends ParticlesTask {
    private final Vector center;
    private final double radiusX;
    private final double radiusZ;
    private final int minY;
    private final int maxY;

    public CylinderParticlesTask(Player player, Cylinder selection) {
        super(player);

        center = selection.getCenter();

        radiusX = selection.getRadiusX() + 0.5D;
        radiusZ = selection.getRadiusZ() + 0.5D;
        minY = selection.getMinY();
        maxY = selection.getMaxY();

        setEdgeColor(PlayerConfig.COLOR_CYLINDER_EDGE.getColor(player.getUniqueId()));
        setFillColor(PlayerConfig.COLOR_CYLINDER_FILL.getColor(player.getUniqueId()));
    }

    @Override
    public void run() {
        calculatePoints();

        super.run();
    }

    public void calculatePoints() {
        if (hasAlreadyCalculated()) {
            return;
        }
        setAlreadyCalculated(true);

        if (center == null) {
            setCancelled(true);
            return;
        }

        List<Vector> bottom = calcEllipse(center.getX(), minY, center.getZ(), radiusX, radiusZ);
        for (Vector vector : bottom) {
            addPacket(vector, getEdgeColor());
        }

        List<Vector> top = calcEllipse(center.getX(), maxY + 1, center.getZ(), radiusX, radiusZ);
        for (Vector vector : top) {
            addPacket(vector, getEdgeColor());
        }

        Set<Vector> grid = new HashSet<>();
        for (int i = 0; i < bottom.size(); i++) {
            grid.addAll(calcLine(top.get(i), bottom.get(i)));
        }

        for (Vector vector : grid) {
            ParticleColor color = getFillColor();
            if (vector.getX() == center.getX() + radiusX ||
                    vector.getX() == center.getX() - radiusX ||
                    vector.getZ() == center.getZ() + radiusZ ||
                    vector.getZ() == center.getZ() - radiusZ) {
                color = getEdgeColor();
            }
            addPacket(vector, color);
        }
    }

    private List<Vector> calcEllipse(double x, int y, double z, double radiusX, double radiusZ) {
        List<Vector> vectors = new ArrayList<>();
        for (double tempX = -Math.ceil(radiusX); tempX <= Math.ceil(radiusX); ++tempX) {
            double tempZ = radiusZ * Math.cos(Math.asin(tempX / radiusX));
            Vector p1 = new Vector(x + tempX, y, z + tempZ);
            vectors.add(p1);
            Vector p2 = new Vector(x + tempX, y, z - tempZ);
            vectors.add(p2);
            for (Vector vector : calcLine(p1, p2)) {
                addPacket(vector, (tempX == 0 || tempZ == 0) ? getEdgeColor() : getFillColor());
            }
        }
        for (double tempZ = -Math.ceil(radiusZ); tempZ <= Math.ceil(radiusZ); ++tempZ) {
            double tempX = radiusX * Math.sin(Math.acos(tempZ / radiusZ));
            Vector p1 = new Vector(x + tempX, y, z + tempZ);
            vectors.add(p1);
            Vector p2 = new Vector(x - tempX, y, z + tempZ);
            vectors.add(p2);
            if (tempX == 0 || tempZ == 0) {
                for (Vector vector : calcLine(p1, p2)) {
                    addPacket(vector, getEdgeColor());
                }
            }
        }
        return vectors;
    }

    private Set<Vector> calcLine(Vector vec1, Vector vec2) {
        Vector p1 = vec1.clone();
        Vector p2 = vec2.clone();
        Set<Vector> vectors = new HashSet<>();
        double length = p1.distance(p2) - 1;
        int points = (int) (length / 1.0D) + 1;
        double gap = length / (points - 1);
        Vector gapVector = p2.clone().subtract(p1).normalize().multiply(gap);
        for (int i = 1; i < points; i++) {
            Vector currentPoint = p1.clone().add(gapVector.clone().multiply(i));
            vectors.add(currentPoint);
        }
        return vectors;
    }
}
