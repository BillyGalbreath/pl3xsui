package net.pl3x.bukkit.pl3xsui.task;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers.Particle;
import net.pl3x.bukkit.pl3xsui.Chat;
import net.pl3x.bukkit.pl3xsui.Logger;
import net.pl3x.bukkit.pl3xsui.Pl3xPlayer;
import net.pl3x.bukkit.pl3xsui.api.IParticleTask;
import net.pl3x.bukkit.pl3xsui.api.ParticleColor;
import net.pl3x.bukkit.pl3xsui.configuration.Config;
import net.pl3x.bukkit.pl3xsui.configuration.Lang;
import net.pl3x.bukkit.pl3xsui.configuration.PlayerConfig;
import net.pl3x.bukkit.pl3xsui.hook.ProtocolLib;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("WeakerAccess")
public abstract class ParticlesTask extends BukkitRunnable implements IParticleTask {
    private final Player player;
    private final ConcurrentHashMap<PacketContainer, Vector> packets = new ConcurrentHashMap<>();
    private boolean longDistance;
    private int radius;
    private int radiusSquared;
    private ParticleColor fillColor;
    private ParticleColor edgeColor;
    private Particle particleType;
    private boolean cancel;
    private boolean alreadyCalculated;
    private boolean running;
    private final Object lock = new Object();

    public ParticlesTask(Player player) {
        this.player = player;

        setLongDistance(Config.VIEW_LONG_DISTANCE.getBoolean());
        setRadius(Config.VIEW_RADIUS.getInt());

        String particleName = PlayerConfig.PARTICLE_TYPE.getString(player.getUniqueId());
        if (!Config.ALLOWED_PLAYER_PARTICLES.getStringList().contains(particleName)) {
            particleName = Config.PARTICLE_TYPE.getString();
        }
        Particle particleType = null;
        try {
            particleType = Particle.valueOf(particleName);
        } catch (IllegalArgumentException e1) {
            try {
                particleType = Particle.valueOf(Config.PARTICLE_TYPE.getString());
            } catch (IllegalArgumentException e2) {
                Logger.error("Particle type in config.yml is invalid. Cancelling particle task.");
                setCancelled(true);
            }
        }
        setParticleType(particleType);
    }

    public Player getPlayer() {
        return player;
    }

    public ConcurrentHashMap<PacketContainer, Vector> getPackets() {
        return packets;
    }

    public boolean isLongDistance() {
        return longDistance;
    }

    public void setLongDistance(boolean longDistance) {
        this.longDistance = longDistance;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
        this.radiusSquared = radius * radius;
    }

    public int getRadiusSquared() {
        return radiusSquared;
    }

    public ParticleColor getFillColor() {
        return fillColor;
    }

    public void setFillColor(ParticleColor fillColor) {
        this.fillColor = fillColor;
    }

    public ParticleColor getEdgeColor() {
        return edgeColor;
    }

    public void setEdgeColor(ParticleColor edgeColor) {
        this.edgeColor = edgeColor;
    }

    public Particle getParticleType() {
        return particleType;
    }

    public void setParticleType(Particle particleType) {
        this.particleType = particleType;
    }

    public boolean isCancelled() {
        return cancel;
    }

    public void setCancelled(boolean cancel) {
        this.cancel = cancel;
    }

    public boolean hasAlreadyCalculated() {
        return alreadyCalculated;
    }

    public void setAlreadyCalculated(boolean alreadyCalculated) {
        this.alreadyCalculated = alreadyCalculated;
    }

    @Override
    public void run() {
        synchronized (lock) {
            if (running) {
                return;
            }
            running = true;
        }

        if (cancel || player == null || !player.isOnline()) {
            cancel();
            return;
        }

        if (crashPrevention()) {
            Logger.error("This particle is known to crash clients!");
            Logger.error("Cancelling spawning attempt!");
            cancel();
            return;
        }

        Set<Player> watchers = new HashSet<>();
        watchers.add(player);
        Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(player);
        watchers.addAll(pl3xPlayer.getWatchers());

        if (packets.size() > Config.PARTICLE_COUNT_THRESHOLD.getInt()) {
            new Chat(Lang.SELECTION_TOO_LARGE).send(player);
            setCancelled(true);
            return;
        }

        for (Player watcher : watchers) {
            if (watcher == null || !watcher.isOnline()) {
                pl3xPlayer.removeWatcher(watcher);
                continue;
            }
            try {
                Vector origin = watcher.getLocation().toVector();

                for (Map.Entry<PacketContainer, Vector> entry : packets.entrySet()) {
                    if (origin.distanceSquared(entry.getValue()) > radiusSquared) {
                        continue; // out of viewing radius range. do not send packet.
                    }

                    ProtocolLib.getManager().sendServerPacket(watcher, entry.getKey());
                }
            } catch (Exception ignore) {
                // ignore any and all exceptions. if anything goes wrong, just ignore this run
            }
        }
        synchronized (lock) {
            running = false;
        }
    }

    @Override
    public void cancel() {
        cancel = true;
        packets.clear();

        super.cancel();
    }

    public void addPacket(Vector vector, ParticleColor color) {
        if (getParticleType() == null) {
            Logger.error("Invalid particle type set in config.yml: " + Config.PARTICLE_TYPE.getString());
            return;
        }

        PacketContainer packet = ProtocolLib.getManager().createPacket(PacketType.Play.Server.WORLD_PARTICLES);

        packet.getParticles().
                write(0, getParticleType());
        packet.getIntegers().
                write(0, 0);                            // amount
        packet.getBooleans().
                write(0, isLongDistance());                 // long distance
        packet.getFloat().
                write(0, (float) vector.getX()).        // x position
                write(1, (float) vector.getY()).        // y position
                write(2, (float) vector.getZ()).        // z position
                write(3, checkColor(color.getRed())).   // offsetX (red)
                write(4, checkColor(color.getGreen())). // offsetY (green)
                write(5, checkColor(color.getBlue())).  // offsetZ (blue)
                write(6, 1F);                           // data (speed)

        packets.put(packet, vector);
    }

    public float checkColor(float color) {
        switch (getParticleType()) {
            case REDSTONE:
            case SPELL_MOB:
                return color;
            default:
                return 0.0F;
        }
    }

    public boolean crashPrevention() {
        switch (getParticleType()) {
            case ITEM_TAKE:
            case ITEM_CRACK:
            case BLOCK_CRACK:
            case BLOCK_DUST:
                return true;
            default:
                return false;
        }
    }
}
