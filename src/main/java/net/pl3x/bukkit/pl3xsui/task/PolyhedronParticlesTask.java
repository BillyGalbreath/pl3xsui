package net.pl3x.bukkit.pl3xsui.task;

import net.pl3x.bukkit.pl3xsui.api.selection.shape.Polyhedron;
import net.pl3x.bukkit.pl3xsui.configuration.PlayerConfig;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SuppressWarnings("WeakerAccess")
public class PolyhedronParticlesTask extends ParticlesTask {
    private final List<Vector> points;
    private final List<Vector> faces;

    public PolyhedronParticlesTask(Player player, Polyhedron selection) {
        super(player);
        this.points = selection.getPoints();
        this.faces = selection.getFaces();

        setEdgeColor(PlayerConfig.COLOR_POLYHEDRON_EDGE.getColor(player.getUniqueId()));
        setFillColor(PlayerConfig.COLOR_POLYHEDRON_FILL.getColor(player.getUniqueId()));
    }

    @Override
    public void run() {
        calculatePoints();

        super.run();
    }

    public void calculatePoints() {
        if (hasAlreadyCalculated()) {
            return;
        }
        setAlreadyCalculated(true);

        if (points == null || points.isEmpty()) {
            setCancelled(true);
            return;
        }

        for (Vector point : points) {
            for (Vector vector : calcBox(point)) {
                addPacket(vector, getEdgeColor());
            }
        }

        for (Vector face : faces) {
            for (Vector vector : calcFace(points.get((int) face.getX()), points.get((int) face.getY()), points.get((int) face.getZ()))) {
                addPacket(vector, getFillColor());
            }
        }
    }

    private Set<Vector> calcBox(Vector point) {
        Set<Vector> box = new HashSet<>();
        box.add(new Vector(point.getX() + 0.5D, point.getY() - 0.5D, point.getZ() + 0.5D));
        box.add(new Vector(point.getX() + 0.5D, point.getY() - 0.5D, point.getZ() - 0.5D));
        box.add(new Vector(point.getX() - 0.5D, point.getY() - 0.5D, point.getZ() + 0.5D));
        box.add(new Vector(point.getX() - 0.5D, point.getY() - 0.5D, point.getZ() - 0.5D));
        box.add(new Vector(point.getX() + 0.5D, point.getY() + 0.5D, point.getZ() + 0.5D));
        box.add(new Vector(point.getX() + 0.5D, point.getY() + 0.5D, point.getZ() - 0.5D));
        box.add(new Vector(point.getX() - 0.5D, point.getY() + 0.5D, point.getZ() + 0.5D));
        box.add(new Vector(point.getX() - 0.5D, point.getY() + 0.5D, point.getZ() - 0.5D));
        return box;
    }

    private Set<Vector> calcFace(Vector p1, Vector p2, Vector p3) {
        Set<Vector> face = new HashSet<>();
        face.addAll(calcLine(p1, p2));
        face.addAll(calcLine(p2, p3));
        face.addAll(calcLine(p3, p1));
        return face;
    }

    private Set<Vector> calcLine(Vector vec1, Vector vec2) {
        Vector p1 = vec1.clone();
        Vector p2 = vec2.clone();
        Set<Vector> vectors = new HashSet<>();
        double length = p1.distance(p2);
        int points = (int) (length / 1.0D) + 1;
        double gap = length / (points - 1);
        Vector gapVector = p2.clone().subtract(p1).normalize().multiply(gap);
        for (int i = 0; i < points; i++) {
            Vector currentPoint = p1.clone().add(gapVector.clone().multiply(i));
            vectors.add(currentPoint);
        }
        return vectors;
    }
}
