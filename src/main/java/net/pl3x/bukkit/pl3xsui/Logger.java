package net.pl3x.bukkit.pl3xsui;

import net.pl3x.bukkit.pl3xsui.configuration.Config;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * Logger
 */
public class Logger {
    private static void log(String msg) {
        msg = ChatColor.translateAlternateColorCodes('&', "&3[&d" + Pl3xSUI.getPlugin(Pl3xSUI.class).getName() + "&3]&r " + msg);
        if (!Config.COLOR_LOGS.getBoolean()) {
            msg = ChatColor.stripColor(msg);
        }
        Bukkit.getServer().getConsoleSender().sendMessage(msg);
    }

    /**
     * Send debug message to console only if debug mode is enabled
     *
     * @param msg Message to send
     */
    public static void debug(String msg) {
        if (Config.DEBUG_MODE.getBoolean()) {
            Logger.log("&7[&eDEBUG&7]&e " + msg);
        }
    }

    /**
     * Send warning message to console
     *
     * @param msg Message to send
     */
    public static void warn(String msg) {
        Logger.log("&e[&6WARN&e]&6 " + msg);
    }

    /**
     * Send error message to console
     *
     * @param msg Message to send
     */
    public static void error(String msg) {
        Logger.log("&e[&4ERROR&e]&4 " + msg);
    }

    /**
     * Send information message to console
     *
     * @param msg Message to send
     */
    public static void info(String msg) {
        Logger.log("&e[&fINFO&e]&r " + msg);
    }
}
