package net.pl3x.bukkit.pl3xsui.command.subcommand;

import com.comphenix.protocol.wrappers.EnumWrappers;
import net.pl3x.bukkit.pl3xsui.Chat;
import net.pl3x.bukkit.pl3xsui.Pl3xPlayer;
import net.pl3x.bukkit.pl3xsui.command.PlayerCommand;
import net.pl3x.bukkit.pl3xsui.configuration.Config;
import net.pl3x.bukkit.pl3xsui.configuration.Lang;
import net.pl3x.bukkit.pl3xsui.configuration.PlayerConfig;
import org.bukkit.command.CommandException;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class CmdParticle extends PlayerCommand {
    public CmdParticle() {
        super("particle", Lang.DESC_CMD_PARTICLE, Lang.HELP_CMD_PARTICLE, "command.pl3xsui.particle");
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        List<String> list = new ArrayList<>();
        if (args.size() == 1) {
            if ("default".startsWith(args.peek().toLowerCase())) {
                list.add("default");
            }
            list.addAll(Config.ALLOWED_PLAYER_PARTICLES.getStringList().stream()
                    .filter(allowed -> allowed.toLowerCase().startsWith(args.peek().toLowerCase()))
                    .map(allowed -> allowed.toLowerCase().replace("_", "-"))
                    .collect(Collectors.toList()));
        }
        return list;
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        String type = args.size() > 0 ? args.pop() : null;

        // reset the color
        if (type == null || type.isEmpty() || type.equalsIgnoreCase("default")) {
            PlayerConfig.PARTICLE_TYPE.set(player.getUniqueId(), "default");
            new Chat(Lang.PARTICLE_TYPE_RESET).send(player);
            Pl3xPlayer.getPlayer(player).showSelection();
            return;
        }

        // check particle exists
        String realName = type.toUpperCase().replace("-", "_");
        EnumWrappers.Particle particle;
        try {
            particle = EnumWrappers.Particle.valueOf(realName);
        } catch (IllegalArgumentException e) {
            new Chat(Lang.PARTICLE_TYPE_DOES_NOT_EXIST).send(player);
            return;
        }

        // check if allowed
        if (!Config.ALLOWED_PLAYER_PARTICLES.getStringList().contains(realName)) {
            new Chat(Lang.PARTICLE_TYPE_NOT_ALLOWED).send(player);
            return;
        }

        // set particle
        PlayerConfig.PARTICLE_TYPE.set(player.getUniqueId(), realName);
        new Chat(Lang.PARTICLE_TYPE_SET
                .replace("{type}", particle.name()))
                .send(player);

        // update current selection
        Pl3xPlayer.getPlayer(player).showSelection();
    }
}
