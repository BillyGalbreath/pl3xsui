package net.pl3x.bukkit.pl3xsui.command;

import net.pl3x.bukkit.pl3xsui.Chat;
import net.pl3x.bukkit.pl3xsui.configuration.Lang;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public abstract class PlayerCommand extends CommandHandler {
    public PlayerCommand(String name, Lang description, Lang help, String permission) {
        super(name, description, help, permission);
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, LinkedList<String> args) {
        if (sender instanceof Player) {
            return onTabComplete((Player) sender, args);
        }
        return new ArrayList<>();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, LinkedList<String> args) throws CommandException {
        if (!(sender instanceof Player)) {
            new Chat(Lang.PLAYER_COMMAND).send(sender);
            return true;
        }
        Player player = (Player) sender;
        if ("?".equals(args.peek()) || "help".equals(args.peek())) {
            showHelp(player);
            return true;
        }
        onCommand(player, args);
        return true;
    }

    @SuppressWarnings("UnusedParameters")
    public abstract List<String> onTabComplete(Player player, LinkedList<String> args);

    public abstract void onCommand(Player player, LinkedList<String> args) throws CommandException;
}
