package net.pl3x.bukkit.pl3xsui.command.subcommand;

import net.pl3x.bukkit.pl3xsui.Chat;
import net.pl3x.bukkit.pl3xsui.Pl3xPlayer;
import net.pl3x.bukkit.pl3xsui.command.PlayerCommand;
import net.pl3x.bukkit.pl3xsui.configuration.Lang;
import net.pl3x.bukkit.pl3xsui.configuration.PlayerConfig;
import org.apache.commons.lang.BooleanUtils;
import org.bukkit.command.CommandException;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class CmdToggle extends PlayerCommand {
    public CmdToggle() {
        super("toggle", Lang.DESC_CMD_TOGGLE, Lang.HELP_CMD_TOGGLE, "command.pl3xsui.toggle");
    }

    @Override
    public List<String> onTabComplete(Player player, LinkedList<String> args) {
        return null;
    }

    @Override
    public void onCommand(Player player, LinkedList<String> args) throws CommandException {
        UUID uuid = player.getUniqueId();

        boolean enable = !PlayerConfig.ENABLED.getBoolean(uuid);
        PlayerConfig.ENABLED.set(uuid, enable);

        Pl3xPlayer.getPlayer(uuid).showSelection();

        new Chat(Lang.SUI_TOGGLED
                .replace("{toggle}", BooleanUtils.toStringOnOff(enable)))
                .send(player);
    }
}
