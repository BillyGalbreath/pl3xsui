package net.pl3x.bukkit.pl3xsui.command;

import net.pl3x.bukkit.pl3xsui.Chat;
import net.pl3x.bukkit.pl3xsui.configuration.Lang;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public abstract class BaseCommand extends CommandHandler {
    private final TreeMap<String, CommandHandler> subCommands = new TreeMap<>();

    protected BaseCommand(String name, Lang description, String permission) {
        super(name, description, null, permission);
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, LinkedList<String> args) {
        if (args.size() > 1) {
            CommandHandler subCmd = subCommands.get(args.pop().toLowerCase());
            if (subCmd != null) {
                if (subCmd.getPermission() == null || sender.hasPermission(subCmd.getPermission())) {
                    return subCmd.onTabComplete(sender, command, args);
                }
            }
            return new ArrayList<>();
        } else {
            return subCommands.entrySet().stream()
                    .filter(cmdPair -> cmdPair.getKey().startsWith(args.peek().toLowerCase()))
                    .filter(cmdPair -> cmdPair.getValue().getPermission() == null || sender.hasPermission(cmdPair.getValue().getPermission()))
                    .map(Map.Entry::getKey).collect(Collectors.toList());
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, LinkedList<String> args) throws CommandException {
        if (args.size() > 0) {
            CommandHandler subCmd = subCommands.get(args.pop().toLowerCase());
            if (subCmd != null) {
                return subCmd.onCommand(sender, command, args);
            }
            new Chat(Lang.UNKNOWN_SUBCOMMAND).send(sender);
        }
        showSubCommands(sender);
        return true;
    }

    private void showSubCommands(CommandSender sender) {
        new Chat(Lang.AVAILABLE_SUBCOMMANDS).send(sender);
        boolean hasSubCmds = false;
        for (CommandHandler handler : subCommands.values()) {
            if (handler.getPermission() == null || sender.hasPermission(handler.getPermission())) {
                new Chat(Lang.COMMAND_DESCRIPTION
                        .replace("{command}", handler.getName())
                        .replace("{description}", handler.getDescription()))
                        .send(sender);
                hasSubCmds = true;
            }
        }
        if (!hasSubCmds) {
            new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
        }
    }

    protected void registerSubcommand(CommandHandler commandHandler) {
        subCommands.put(commandHandler.getName().toLowerCase(), commandHandler);
    }
}
