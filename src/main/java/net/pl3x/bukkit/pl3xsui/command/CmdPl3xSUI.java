package net.pl3x.bukkit.pl3xsui.command;

import net.pl3x.bukkit.pl3xsui.Pl3xSUI;
import net.pl3x.bukkit.pl3xsui.command.subcommand.CmdColor;
import net.pl3x.bukkit.pl3xsui.command.subcommand.CmdParticle;
import net.pl3x.bukkit.pl3xsui.command.subcommand.CmdReload;
import net.pl3x.bukkit.pl3xsui.command.subcommand.CmdToggle;
import net.pl3x.bukkit.pl3xsui.command.subcommand.CmdWatching;
import net.pl3x.bukkit.pl3xsui.configuration.Lang;

public class CmdPl3xSUI extends BaseCommand {
    public CmdPl3xSUI(Pl3xSUI plugin) {
        super("pl3xsui", Lang.DESC_CMD_PL3XSUI, null);
        registerSubcommand(new CmdColor());
        registerSubcommand(new CmdParticle());
        registerSubcommand(new CmdReload(plugin));
        registerSubcommand(new CmdToggle());
        registerSubcommand(new CmdWatching(plugin));
    }
}
