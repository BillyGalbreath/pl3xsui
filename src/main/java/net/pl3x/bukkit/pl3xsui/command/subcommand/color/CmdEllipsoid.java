package net.pl3x.bukkit.pl3xsui.command.subcommand.color;

import net.pl3x.bukkit.pl3xsui.Chat;
import net.pl3x.bukkit.pl3xsui.Pl3xPlayer;
import net.pl3x.bukkit.pl3xsui.api.ParticleColor;
import net.pl3x.bukkit.pl3xsui.command.BaseCommand;
import net.pl3x.bukkit.pl3xsui.command.PlayerCommand;
import net.pl3x.bukkit.pl3xsui.configuration.Lang;
import net.pl3x.bukkit.pl3xsui.configuration.PlayerConfig;
import org.bukkit.command.CommandException;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class CmdEllipsoid extends BaseCommand {
    public CmdEllipsoid() {
        super("ellipsoid", Lang.DESC_CMD_COLOR_ELLIPSOID, null);
        registerSubcommand(new CmdFill());
        registerSubcommand(new CmdEdge());
    }

    public class CmdFill extends PlayerCommand {
        private CmdFill() {
            super("fill", Lang.DESC_CMD_COLOR_ELLIPSOID_FILL, Lang.HELP_CMD_COLOR_ELLIPSOID_FILL, null);
        }

        @Override
        public List<String> onTabComplete(Player player, LinkedList<String> args) {
            List<String> list = new ArrayList<>();
            if (args.size() == 1) {
                if ("default".startsWith(args.peek().toLowerCase())) {
                    list.add("default");
                }
                list.addAll(ParticleColor.getNames().stream()
                        .filter(name -> name.startsWith(args.peek().toLowerCase()))
                        .collect(Collectors.toList()));
            }
            return list;
        }

        @Override
        public void onCommand(Player player, LinkedList<String> args) throws CommandException {
            String fillColor = args.size() > 0 ? args.pop() : null;

            // reset the color
            if (fillColor == null || fillColor.isEmpty() || fillColor.equalsIgnoreCase("default")) {
                PlayerConfig.COLOR_ELLIPSOID_FILL.set(player.getUniqueId(), "default");
                new Chat(Lang.ELLIPSOID_COLOR_FILL_RESET).send(player);
                Pl3xPlayer.getPlayer(player).showSelection();
                return;
            }

            // check color exists
            String realName = fillColor.toLowerCase().replace("_", "-");
            ParticleColor color = ParticleColor.getColorExact(realName);
            if (color == null) {
                new Chat(Lang.COLOR_DOES_NOT_EXIST).send(player);
                return;
            }

            // set color
            PlayerConfig.COLOR_ELLIPSOID_FILL.set(player.getUniqueId(), realName);
            new Chat(Lang.ELLIPSOID_COLOR_FILL_SET
                    .replace("{color}", color.getHex()))
                    .send(player);

            // update current selection
            Pl3xPlayer.getPlayer(player).showSelection();
        }
    }

    public class CmdEdge extends PlayerCommand {
        private CmdEdge() {
            super("edge", Lang.DESC_CMD_COLOR_ELLIPSOID_EDGE, Lang.HELP_CMD_COLOR_ELLIPSOID_EDGE, null);
        }

        @Override
        public List<String> onTabComplete(Player player, LinkedList<String> args) {
            List<String> list = new ArrayList<>();
            if (args.size() == 1) {
                if ("default".startsWith(args.peek().toLowerCase())) {
                    list.add("default");
                }
                list.addAll(ParticleColor.getNames().stream()
                        .filter(name -> name.startsWith(args.peek().toLowerCase()))
                        .collect(Collectors.toList()));
            }
            return list;
        }

        @Override
        public void onCommand(Player player, LinkedList<String> args) throws CommandException {
            String edgeColor = args.size() > 0 ? args.pop() : null;

            // reset the color
            if (edgeColor == null || edgeColor.isEmpty() || edgeColor.equalsIgnoreCase("default")) {
                PlayerConfig.COLOR_ELLIPSOID_EDGE.set(player.getUniqueId(), "default");
                new Chat(Lang.ELLIPSOID_COLOR_EDGE_RESET).send(player);
                Pl3xPlayer.getPlayer(player).showSelection();
                return;
            }

            // check color exists
            String realName = edgeColor.toLowerCase().replace("_", "-");
            ParticleColor color = ParticleColor.getColorExact(realName);
            if (color == null) {
                new Chat(Lang.COLOR_DOES_NOT_EXIST).send(player);
                return;
            }

            // set color
            PlayerConfig.COLOR_ELLIPSOID_EDGE.set(player.getUniqueId(), realName);
            new Chat(Lang.ELLIPSOID_COLOR_EDGE_SET
                    .replace("{color}", color.getHex()))
                    .send(player);

            // update current selection
            Pl3xPlayer.getPlayer(player).showSelection();
        }
    }
}

