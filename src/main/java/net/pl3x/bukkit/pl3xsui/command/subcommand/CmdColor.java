package net.pl3x.bukkit.pl3xsui.command.subcommand;

import net.pl3x.bukkit.pl3xsui.command.BaseCommand;
import net.pl3x.bukkit.pl3xsui.command.subcommand.color.CmdCuboid;
import net.pl3x.bukkit.pl3xsui.command.subcommand.color.CmdCylinder;
import net.pl3x.bukkit.pl3xsui.command.subcommand.color.CmdEllipsoid;
import net.pl3x.bukkit.pl3xsui.command.subcommand.color.CmdPolygon2D;
import net.pl3x.bukkit.pl3xsui.command.subcommand.color.CmdPolyhedron;
import net.pl3x.bukkit.pl3xsui.command.subcommand.color.CmdSphere;
import net.pl3x.bukkit.pl3xsui.configuration.Lang;

public class CmdColor extends BaseCommand {
    public CmdColor() {
        super("color", Lang.DESC_CMD_COLOR, "command.pl3xsui.color");
        registerSubcommand(new CmdCuboid());
        registerSubcommand(new CmdCylinder());
        registerSubcommand(new CmdSphere());
        registerSubcommand(new CmdEllipsoid());
        registerSubcommand(new CmdPolygon2D());
        registerSubcommand(new CmdPolyhedron());
    }
}
