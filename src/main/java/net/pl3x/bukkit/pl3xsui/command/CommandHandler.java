package net.pl3x.bukkit.pl3xsui.command;

import net.pl3x.bukkit.pl3xsui.Chat;
import net.pl3x.bukkit.pl3xsui.configuration.Lang;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public abstract class CommandHandler implements TabExecutor {
    private final String name;
    private final String description;
    private final String help;
    private final String permission;

    protected CommandHandler(String name, Lang description, Lang help, String permission) {
        this.name = name;
        this.description = description.toString();
        this.help = help == null ? null : help.toString();
        this.permission = permission;
    }

    void showHelp(CommandSender sender) {
        if (getHelp() != null) {
            for (String msg : getHelp().split("\\n")) {
                new Chat(msg).send(sender);
            }
        }
    }

    String getName() {
        return name;
    }

    String getDescription() {
        return description;
    }

    @SuppressWarnings("WeakerAccess")
    String getHelp() {
        return help;
    }

    String getPermission() {
        return permission;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        try {
            return onCommand(sender, command, new LinkedList<>(Arrays.asList(args)));
        } catch (CommandException e) {
            if (e.getMessage() == null || e.getMessage().equals("")) {
                new Chat(Lang.UNKNOWN_ERROR).send(sender);
            } else {
                new Chat(e.getMessage()).send(sender);
            }
            return true;
        }
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] strings) {
        return onTabComplete(sender, command, new LinkedList<>(Arrays.asList(strings)));
    }

    protected abstract List<String> onTabComplete(CommandSender sender, Command command, LinkedList<String> args);

    protected abstract boolean onCommand(CommandSender sender, Command command, LinkedList<String> args) throws CommandException;
}
