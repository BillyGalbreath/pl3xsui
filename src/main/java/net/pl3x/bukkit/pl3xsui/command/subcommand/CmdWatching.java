package net.pl3x.bukkit.pl3xsui.command.subcommand;

import net.pl3x.bukkit.pl3xsui.Chat;
import net.pl3x.bukkit.pl3xsui.Pl3xPlayer;
import net.pl3x.bukkit.pl3xsui.Pl3xSUI;
import net.pl3x.bukkit.pl3xsui.command.BaseCommand;
import net.pl3x.bukkit.pl3xsui.command.PlayerCommand;
import net.pl3x.bukkit.pl3xsui.configuration.Lang;
import org.bukkit.command.CommandException;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CmdWatching extends BaseCommand {
    private final Pl3xSUI plugin;

    public CmdWatching(Pl3xSUI plugin) {
        super("watching", Lang.DESC_CMD_WATCHING, "command.pl3xsui.watching");
        this.plugin = plugin;
        registerSubcommand(new CmdList());
        registerSubcommand(new CmdAdd());
        registerSubcommand(new CmdRemove());
    }

    public class CmdList extends PlayerCommand {
        private CmdList() {
            super("list", Lang.DESC_CMD_WATCHING_LIST, Lang.HELP_CMD_WATCHING_LIST, null);
        }

        @Override
        public List<String> onTabComplete(Player player, LinkedList<String> args) {
            return new ArrayList<>();
        }

        @Override
        public void onCommand(Player player, LinkedList<String> args) throws CommandException {
            Set<String> watching = new HashSet<>();
            watching.addAll(Pl3xPlayer.getPlayers().stream()
                    .filter(pl3xPlayer -> pl3xPlayer.isWatching(player))
                    .map(pl3xPlayer -> pl3xPlayer.getPlayer().getName())
                    .collect(Collectors.toList()));
            String list = String.join(" ", watching);
            if (list == null || list.isEmpty()) {
                new Chat(Lang.NOT_WATCHING_ANY_PLAYERS).send(player);
                return;
            }
            new Chat(Lang.WATCHING_PLAYERS
                    .replace("{players}", list))
                    .send(player);
        }
    }

    public class CmdAdd extends PlayerCommand {
        private CmdAdd() {
            super("add", Lang.DESC_CMD_WATCHING_ADD, Lang.HELP_CMD_WATCHING_ADD, null);
        }

        @Override
        public List<String> onTabComplete(Player player, LinkedList<String> args) {
            List<String> list = new ArrayList<>();
            if (args.size() == 1) {
                list.addAll(plugin.getServer().getOnlinePlayers().stream()
                        .filter(target -> target.getName().toLowerCase().startsWith(args.peek().toLowerCase()))
                        .map(Player::getName).collect(Collectors.toList()));
            }
            return list;
        }

        @Override
        public void onCommand(Player player, LinkedList<String> args) throws CommandException {
            if (args.size() == 0) {
                new Chat(Lang.PLAYER_NOT_SPECIFIED).send(player);
                return;
            }
            //noinspection deprecation
            Player target = plugin.getServer().getPlayer(args.pop());
            if (target == null) {
                new Chat(Lang.PLAYER_NOT_FOUND).send(player);
                return;
            }
            Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(target);
            if (pl3xPlayer.isWatching(target)) {
                new Chat(Lang.ALREADY_WATCHING_PLAYER
                        .replace("{player}", target.getName()))
                        .send(player);
                return;
            }
            pl3xPlayer.addWatcher(player);
            new Chat(Lang.STARTED_WATCHING_PLAYER
                    .replace("{player}", target.getName()))
                    .send(player);
        }
    }

    public class CmdRemove extends PlayerCommand {
        private CmdRemove() {
            super("remove", Lang.DESC_CMD_WATCHING_REMOVE, Lang.HELP_CMD_WATCHING_REMOVE, null);
        }

        @Override
        public List<String> onTabComplete(Player player, LinkedList<String> args) {
            List<String> list = new ArrayList<>();
            if (args.size() == 1) {
                list.addAll(plugin.getServer().getOnlinePlayers().stream()
                        .filter(target -> target.getName().toLowerCase().startsWith(args.peek().toLowerCase()))
                        .map(Player::getName).collect(Collectors.toList()));
            }
            return list;
        }

        @Override
        public void onCommand(Player player, LinkedList<String> args) throws CommandException {
            if (args.size() == 0) {
                new Chat(Lang.PLAYER_NOT_SPECIFIED).send(player);
                return;
            }
            //noinspection deprecation
            Player target = plugin.getServer().getPlayer(args.pop());
            if (target == null) {
                new Chat(Lang.PLAYER_NOT_FOUND).send(player);
                return;
            }
            Pl3xPlayer pl3xPlayer = Pl3xPlayer.getPlayer(target);
            if (!pl3xPlayer.isWatching(target)) {
                new Chat(Lang.NOT_WATCHING_PLAYER
                        .replace("{player}", target.getName()))
                        .send(player);
                return;
            }
            pl3xPlayer.removeWatcher(player);
            new Chat(Lang.STOPPED_WATCHING_PLAYER
                    .replace("{player}", target.getName()))
                    .send(player);
        }
    }
}
