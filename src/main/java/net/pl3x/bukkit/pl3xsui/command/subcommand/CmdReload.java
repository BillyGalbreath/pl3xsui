package net.pl3x.bukkit.pl3xsui.command.subcommand;

import net.pl3x.bukkit.pl3xsui.Chat;
import net.pl3x.bukkit.pl3xsui.Pl3xPlayer;
import net.pl3x.bukkit.pl3xsui.Pl3xSUI;
import net.pl3x.bukkit.pl3xsui.command.CommandHandler;
import net.pl3x.bukkit.pl3xsui.configuration.Config;
import net.pl3x.bukkit.pl3xsui.configuration.Lang;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;
import java.util.List;

public class CmdReload extends CommandHandler {
    private final Pl3xSUI plugin;

    public CmdReload(Pl3xSUI plugin) {
        super("reload", Lang.DESC_CMD_RELOAD, Lang.HELP_CMD_RELOAD, "command.pl3xsui.reload");
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, LinkedList<String> args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, LinkedList<String> args) throws CommandException {
        Lang.reload(true);
        Config.reload();

        for (Player player : plugin.getServer().getOnlinePlayers()) {
            Pl3xPlayer.getPlayer(player).showSelection();
        }

        new Chat(Lang.RELOAD
                .replace("{plugin}", plugin.getName())
                .replace("{version}", plugin.getDescription().getVersion()))
                .send(sender);
        return true;
    }
}
